#include "Blob.h"


double dist_Blob_min_img_x_y(BLOB blob1, BLOB blob2, double box_x, double box_x_2, double box_y, double box_y_2)
{
	double dx = blob1[0]- blob2[0];

	double dy = blob1[1] - blob2[1];

	double dz = blob1[2] - blob2[2];
	
	if (dx < -box_x_2)
	{
		dx += box_x;
	}
	else if (dx > box_x_2)
	{
		dx -= box_x;

	}
	if (dy < -box_y_2)
	{
		dy += box_y;
	}
	else if (dy > box_y_2)
	{
		dy -= box_y;

	}


	/*
	dx = dx - box_x * round(dx / box_x);
	dy = dy - box_y * round(dy / box_y);
	*/
	//distij(1:2) = distij(1:2) - lbox(1:2) * nint(distij(1:2) / lbox(1:2))

	return sqrt(dx * dx + dy * dy + dz * dz);
}


double dist_2_Blob_min_img_x_y(BLOB blob1, BLOB blob2, double box_x, double box_x_2, double box_y, double box_y_2)
{
	double dx = blob1[0] - blob2[0];

	double dy = blob1[1] - blob2[1];

	double dz = blob1[2] - blob2[2];
	
	if (dx < -box_x_2)
	{
		dx += box_x;
	}
	else if (dx > box_x_2)
	{
		dx -= box_x;

	}
	if (dy < -box_y_2)
	{
		dy += box_y;
	}
	else if (dy > box_y_2)
	{
		dy -= box_y;

	}


	/*
	dx = dx - box_x * round(dx / box_x);
	dy = dy - box_y * round(dy / box_y);
	*/

	//distij(1:2) = distij(1:2) - lbox(1:2) * nint(distij(1:2) / lbox(1:2))

	return dx * dx + dy * dy + dz * dz;
}


BLOB Random_move(BLOB blob1, double max_move_blob, double box_x, double  box_y)
{
	double ranx = max_move_blob * RandomDouble11();
	double new_x = blob1[0] + ranx;

	double rany = max_move_blob * RandomDouble11();

	double new_y = blob1[1] + rany;
	if (new_x < 0)
	{
		new_x += box_x;
	}
	else if (new_x > box_x)
	{
		new_x -= box_x;

	}
	if (new_y < 0)
	{
		new_y += box_y;
	}
	else if (new_y > box_y)
	{
		new_y -= box_y;

	}
	/*
	new_x = new_x - box_x * floor(new_x / box_x);
	new_y = new_y - box_y * floor(new_y / box_y);
	*/
	double new_z;
	double ranz = max_move_blob * RandomDouble11();

	new_z = blob1[2] + ranz;
	BLOB out_blob{ new_x ,new_y ,new_z };
	return out_blob;
}



double Random_dist_Bond_Blob_Blob()
{
	double xm;
	double ym;
	while (true)
	{
		xm = RandomDouble01() * 6.;
		ym = RandomDouble01() * 1.8;

		if (xm * xm * exp(-interaction_Blob_Blob_bond(xm)) >= ym)
		{
			break;
		}
	}
	return xm; // distance distribution using rejection method
}


void random_point_unit_sphere_MAR(double out_xyz[3])
{
	double x1;
	double x2;
	double x1_x2_2;
	while (true)
	{
		x1 = RandomDouble11();
		x2 = RandomDouble11();
		x1_x2_2 = x1 * x1 + x2 * x2;
		if (x1_x2_2 < 1)
		{
			break;
		}
	}

	double sqrt_x1_x2_2 = sqrt(1 - x1_x2_2);

	out_xyz[0] =   2 * x1 * sqrt_x1_x2_2;
	out_xyz[1] =   2 * x2 * sqrt_x1_x2_2;
	out_xyz[2] =   (1 - 2 * x1_x2_2);

}




double rejection_sampling_theta(double max_P_theta,double eps_b) 
{
	// theta from a distribuion of sin(theta)*exp(-u_bend(theta,eps_b)) with rejection sampling
	while (true)
	{
		double theta_trial = RandomDouble01() * pi;
		double y = RandomDouble01() * max_P_theta;

		double u_bend = interaction_cos_theta_bending(eps_b, cos(theta_trial));
		
		if (y <= sin(theta_trial) * exp(-u_bend))
			return theta_trial;
	}

}

BLOB random_point_sphere_theta_prev_bond(double theta,BLOB prev_bond_unit_vector)
{
	double phi = RandomDouble11() * pi;   // phi uniformly distributed


	BLOB non_rot_xyz = sphere_to_cart(1, phi, theta); // cartesian coordinates of point facing away from point [0,0,-1]

	BLOB vec_0 = {0,0,1};
	MATRIX33 rot_mat = get_rot_matrix_a_to_b(vec_0, prev_bond_unit_vector); // rotation matrix that rotates [0,0,1] to the previous bond vector

	BLOB xyz = multi_31_matrix_vector(rot_mat, non_rot_xyz);

	return xyz;


}



MATRIX33 get_rot_matrix_a_to_b(BLOB a, BLOB b) // rotation matrix that rotates unit vector a onto unit vector b
{
	// https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d/476311#476311
	
	BLOB v = cross_product(a, b); //v = a x b
	//s = lenght(v)
	double c = dot_prod(a, b);
	
	MATRIX33 I = { { {{1,0,0}}, {{0,1,0}} , {{0,0,1}} } };

	MATRIX33 vx = { { {{0, -v[2], v[1]}}, {{v[2], 0, -v[0]}} , {{-v[1], v[0], 0}} } };
		
	MATRIX33 vx_vx = multi_33_matrix(vx, vx);

	MATRIX33 R{};

	double cons = 1. / (1. + c);
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			//R = I + vx + vx_vx * 1 / (1 + c)

			R[i][j] = I[i][j] + vx[i][j] + vx_vx[i][j] * cons;
		}
	}


	return R;
}


MATRIX33 multi_33_matrix(MATRIX33 matrix1, MATRIX33 matrix2)
{
	MATRIX33 result_matrix{};
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			double product = 0;
			for (int k = 0; k < 3; k++) {
				product += matrix1[i][k] * matrix2[k][j];
			}
			result_matrix[i][j] = product;

		}
	}
	return result_matrix;
}

BLOB multi_31_matrix_vector(MATRIX33 matrix1, BLOB vector)
{
	BLOB result_vector;
	for (int i = 0; i < 3; i++) 
	{
		double product = 0;

		for (int j = 0; j < 3; j++) 
		{
				product += matrix1[i][j] * vector[j];
		}
		result_vector[i] = product;	
	}
	return result_vector;
}


BLOB sphere_to_cart(double r, double phi, double theta)
{
	BLOB out_xyz{};
	// spherical to cartesian coords
	out_xyz[0] = r * sin(theta) * cos(phi);
	out_xyz[1] = r * sin(theta) * sin(phi);
	out_xyz[2] = r * cos(theta);

	return out_xyz;
}

BLOB cross_product(BLOB v_A, BLOB v_B)
{
	BLOB out_vec;
	out_vec[0] = v_A[1] * v_B[2] - v_A[2] * v_B[1];
	out_vec[1] = -(v_A[0] * v_B[2] - v_A[2] * v_B[0]);
	out_vec[2] = v_A[0] * v_B[1] - v_A[1] * v_B[0];
	return out_vec;
}




BLOB Random_Blob_close_sphere(BLOB blob1,double box_x, double  box_y)
{
	

	//random distance
double rand_bond_dist = Random_dist_Bond_Blob_Blob();

// random point on sphere - Marsagila

double unit_sphere_xyz[3]{};

double z_new;
double x_new;
double y_new;

random_point_unit_sphere_MAR(unit_sphere_xyz);
x_new = blob1[0] + rand_bond_dist * unit_sphere_xyz[0];


y_new = blob1[1] + rand_bond_dist * unit_sphere_xyz[1];


//           posblob(1:2,iiblob,iichain)=posblob(1:2,iiblob,iichain)-lbox(1:2)*	floor(posblob(1:2, iiblob, iichain) / lbox(1:2))
if (x_new < 0)
{
	x_new += box_x;
}
else if (x_new > box_x)
{
	x_new -= box_x;

}
if (y_new < 0)
{
	y_new += box_y;
}
else if (y_new > box_y)
{
	y_new -= box_y;

}

z_new = blob1[2] + rand_bond_dist * unit_sphere_xyz[2];
BLOB out_blob = { x_new, y_new, z_new };

return out_blob;
}



double calc_max_P_theta(double eps_b)
{
	//max_theta = 2*m.atan(np.sqrt(np.sqrt(4*eps_b**2+1)-2*eps_b)) # theta where p(theta) is max
	double max_theta = 2 * atan(sqrt(sqrt(4 * eps_b * eps_b + 1) - 2 * eps_b)); // theta where p(theta) is max
	;

	return sin(max_theta)* exp(-interaction_cos_theta_bending(eps_b, cos(max_theta)));

}


BLOB Random_Blob_close_sphere_theta_bias(BLOB current_blob, BLOB prev_blob,double box_x, double  box_y, double max_p_theta, double eps_b)
{

	//random distance
	double rand_bond_dist = Random_dist_Bond_Blob_Blob();
	
	// random point on sphere - with theta U_bend bias


	double theta = rejection_sampling_theta(max_p_theta, eps_b); // get theta with rejection sampling

	BLOB prev_bond_unit_vector = { {current_blob[0]- prev_blob[0],current_blob[1] - prev_blob[1],current_blob[2] - prev_blob[2]}};

	double l = lenght(prev_bond_unit_vector);

	for (int i = 0; i < 3; i++)
	{
		prev_bond_unit_vector[i] /= l;
	}

	BLOB unit_sphere_xyz = random_point_sphere_theta_prev_bond(theta, prev_bond_unit_vector); // choose phi uniformly, rotate vector according to previous bond vector


	double z_new;
	double x_new;
	double y_new;

	x_new = current_blob[0] + rand_bond_dist * unit_sphere_xyz[0];
	y_new = current_blob[1] + rand_bond_dist * unit_sphere_xyz[1];


	//           posblob(1:2,iiblob,iichain)=posblob(1:2,iiblob,iichain)-lbox(1:2)*	floor(posblob(1:2, iiblob, iichain) / lbox(1:2))
	if (x_new < 0)
	{
		x_new += box_x;
	}
	else if (x_new > box_x)
	{
		x_new -= box_x;

	}
	if (y_new < 0)
	{
		y_new += box_y;
	}
	else if (y_new > box_y)
	{
		y_new -= box_y;

	}
	/*
	x_new = x_new - box_x * floor(x_new / box_x);
	y_new = y_new - box_y * floor(y_new / box_y);
	*/
	z_new = current_blob[2] + rand_bond_dist * unit_sphere_xyz[2];
	BLOB out_blob = { x_new, y_new, z_new };

	return out_blob;
}


double dot_prod(BLOB vec1, BLOB vec2) // BLOB used as a vector 3
{
	return vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2];
}

double lenght(BLOB vec)
{
	return sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
}




double interaction_Blob_Blob(double r)
{
	return 1.75 * exp(-0.8 * r * r);
}
double interaction_r2_Blob_Blob(double r2)
{
	return 1.75 * exp(-0.8 * r2);
}
double interaction_Blob_Blob_bond(double r)
{
	return 0.534 * pow((r - 0.73), 2);
}
double interaction_Blob_Wall(double r)
{
	return 3.2 * exp(-4.17 * (r - 0.5));
}
double interaction_r2_Bond_Tether(double r2)
{
	return 0.75 * r2;
}


double interaction_cos_theta_bending(double eps_b, double cos_theta)
{
	return eps_b * (1 - cos_theta);
}



double Lennard_Jones_r2(double r2, double sigma2, double epsilon)
{
	double r6 = pow(sigma2 / r2, 3);
	return 4 * epsilon * (r6 * r6 - r6);
}

