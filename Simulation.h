#pragma once
#include "Blob.h"
#include <string>
#include <fstream>
#include <chrono>
#include <array>
#include <stdio.h>
#include <iomanip> 



const int max_poly_in_sim = 200;
const int max_k = 10;
const int max_blobs_in_poly = 120;

const int max_cells = 100;
const int max_blobs_in_cell = 50;
const int max_rec_in_cell = 200;

const int max_total_blobs = max_blobs_in_poly * max_poly_in_sim;

extern std::array<std::array<std::array<std::array<int, max_blobs_in_cell>, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_ids;
extern std::array< std::array<int, 3>, max_total_blobs> cell_list_id_cell_xyz;
extern std::array<std::array<std::array<int, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_N_cell;
extern std::array<POLYMER, max_poly_in_sim> Polymers;

const int total_nr_g_r_rec_rec_bins = 120;//100;
const int total_nr_dist_bins = 100;
const int total_nr_cos_theta_bins = 200;
const int total_nr_theta_bins = total_nr_cos_theta_bins;

const int cell_list_around = 1;
const double cell_list_ideal_side_rcut_div = 1.0;


const int max_N_rec = 10000;
extern std::array<REC, max_N_rec> Receptors;
extern std::array<std::array<int, 3>, max_N_rec> Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}

extern std::array<int, max_poly_in_sim> Ligands_N_in_Poly;
const int max_Rec_types = 5;
const int max_ligands_in_poly = 120;

//  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
extern std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_in_Poly;
// Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
extern std::array<std::array<std::array<int, max_ligands_in_poly + 1>, max_blobs_in_poly>, max_poly_in_sim> Ligands_in_Blob;

extern std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_Types_Bonds; // Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}

extern std::array<std::array<int, 2>, max_N_rec> Rec_Cell_List_rid_cell_xy;
extern std::array<std::array<std::array<int, max_rec_in_cell>, max_cells>, max_cells> Rec_Cell_List_cell_xy_rid;
extern std::array<std::array<int, max_cells>, max_cells> Rec_Cell_List_cell_xy_N;


extern std::array<int, max_poly_in_sim> N_bonds_in_chain;



class Simulation
{
public:

	int nr_Poly = 0;
	double total_nr_cycles = 0; //init from input
	double out_frac_cycles = 0.05; //init from input
	int k = 8; //init from input
	double mu = -7.5; //init from input
	double exp_mu = exp(mu);
	double ekv_frac = 0.5;
	double total_U = 0;
	uint64_t ekv_cycles = (uint64_t)(total_nr_cycles * ekv_frac + 0.5);


	double rcut_Blob_Blob = 3.0;
	double rcut_Blob_Blob_2 = rcut_Blob_Blob * rcut_Blob_Blob;

	double rcut_Blob_Wall = 2.0;

	double rcut_Lig_Rec = 2; //init from input
	double rcut_Lig_Rec_2 = rcut_Lig_Rec * rcut_Lig_Rec;//init from input

	double pot_shift_Blob_Blob = interaction_Blob_Blob(rcut_Blob_Blob);
	double pot_shift_Blob_Wall = interaction_Blob_Wall(rcut_Blob_Wall);
	//double pot_shift_U_tether = interaction_r2_Bond_Tether(rcut_Lig_Rec_2);//init from input

	double eps_b = 0.0; //init from input

	double cyc = 0;
	double stp = 0;

	double box_x = 50; //init from input
	double box_y = 50;
	double box_z = 50;

	double max_move_rec = 1; //init from input
	double max_move_blob = 1; //init from input
	int random_seed; //init from input

	double box_x_2 = box_x / 2.;
	double box_y_2 = box_y / 2.;
	double box_z_2 = box_z / 2.;

	int Blobs_in_poly = 10; //init from input

	double frac_ins_del_moves = 0.02; //init from input
	double frac_rec_moves = 0.02; //init from input

	bool exit_signal = false;
	Simulation(std::string filename);
	Simulation();
	void Read_initail_conf_poly();

	void Init_consts();

	double Calculate_Interactions_Blob(int poly_id, int blob_id, BLOB curr_trial_blob); //Calculates interactions between a blob and other blobs (using cell list), does not count blob with poly_id blob_id

	double Total_Energy();

	void MC_move_Blob(); // Standard N,V,T Monte Carlo move, with ligand receptor binding
	double nr_acc_MC_move_Blob = 0;
	double nr_acc_MC_RB_ins = 0;
	double nr_acc_MC_RB_del = 0;

	double nr_acc_MC_move_Blob_cycle = 0;
	double nr_acc_MC_RB_ins_cycle = 0;
	double nr_acc_MC_RB_del_cycle = 0;

	double nr_acc_MC_move_Rec = 0;
	double nr_acc_MC_move_Rec_cycle = 0;

	double nr_tot_MC_move_Blob = 0;
	double nr_tot_MC_RB_ins = 0;
	double nr_tot_MC_RB_del = 0;

	double nr_tot_MC_move_Blob_cycle = 0;
	double nr_tot_MC_RB_ins_cycle = 0;
	double nr_tot_MC_RB_del_cycle = 0;


	double nr_tot_MC_move_Rec = 0;
	double nr_tot_MC_move_Rec_cycle = 0;


	bool do_block_copoly_indent = true;

	bool output_last_conf = false;//init from input
	bool more_outputs = false;//init from input
	int block_copoly = 0; //init from input
	bool poiss_ligand_dist = false; //init from input


	void RB_Insert(); // Rosenbluth/ conf bias Insert Polymer move
	void RB_Delete(); // Rosenbluth/ conf bias Delete Polymer move
	double Calculate_Ext_Interactions_Blob(POLYMER trial_poly, BLOB trial_blob, int n_trial_blobs, int old_poly_id);//Calculates only external (no Blob-Blob harmonic) interactions between a blob and other blobs (using cell list)
	//	n_trial_blobs tells how many blobs are already in the trial poly being built up by RB sampling, does not count old_poly_id

	void RB_Ins_Del_MC_move(); // Chooses between  RB_Insert and RB_Delete

	void Run_Sim(); //Runs the simulation with chosen parametrs


	// Output functions
	void Output(uint64_t cycle, double avg_nr_poly_out_cycle, double avg_U_total_per_poly_out_cycle, double exe_time, double avg_nr_Bonds_per_poly_per_out_cycle, double nr_Bound_Poly_out_cycles_sum);
	void Start_Output();
	void End_Output(double avg_nr_poly, double avg_U_total_per_poly, double total_exe_time, double avg_nr_Bonds_per_poly, std::array<double, max_Rec_types + 1> nr_Bonds_Lig_cycle_sum, std::array<double, max_Rec_types + 1> nr_Bonds_Rec_cycle_sum, double avg_nr_Bound_Poly);
	void Output_system(std::string& filename);
	void Output_bonds(std::string& filename);


	// Cell List functions
	std::array<double, 3> cell_side;
	std::array<int, 3> N_cells;

	void Make_Cell_List(); // initailses cell list
	void Check_Cell_List(); // checks cell list, writes to log.txt




	double max_P_theta = 1; // initalise with eps_b

	double calc_cos_bond_angle(BLOB blob_0, BLOB blob_1, BLOB blob_2);

	int nr_Rec_types = 2; //init from input
	double Rec_HS_rad = 1; //init from input
	double total_N_Rec = 50; //init from input
	std::array<int, max_Rec_types + 1> N_rec_per_type = {}; //init from input
	
	void init_Rec_lists();

	void init_Receptors();
	std::array<double, 2> rec_cell_side = {};
	std::array<int, 2> rec_N_cells = {};


	double dist_2_min_img_xy_blob_rec(BLOB blob, REC rec);

	int Find_Rec_near_Blob(int out_rec_ids[], double out_rec_dists2[], BLOB blob); // 
	double dist_2_min_img_xy_rec_rec(REC rec1, REC rec2);
	bool Check_Rec_Overlap(REC current_trial_rec, int current_rec_id);

	bool do_Rec_LJ_inter = false;
	void MC_move_Rec_HS();
	void MC_move_Rec_LJ();
	double total_rec_U = 0;
	double Total_Rec_Energy();

	double rcut_Rec_Rec = rcut_Lig_Rec; // init from input 
	double rcut_Rec_Rec_2 = rcut_Rec_Rec * rcut_Rec_Rec; // init from input 
	
	std::array<std::array<double, max_Rec_types + 1>, max_Rec_types + 1> Rec_Rec_inter_matrix = {}; //init from input
	std::array<std::array<double, max_Rec_types + 1>, max_Rec_types + 1> pot_shift_Rec_Rec_matrix = {}; //init from input 
	double sigma_rec = 0.2;
	double sigma_rec_2 = sigma_rec * sigma_rec; //init from input


	double Rec_HS_overlap_2 = 4 * Rec_HS_rad * Rec_HS_rad; //init from input

	double nr_Bonds = 0;
	double nr_Bound_Poly = 0;
	std::array<double, max_Rec_types + 1> nr_Bonds_Rec = {};
	std::array<double, max_Rec_types + 1> nr_Bonds_Lig = {};

	

	std::array<std::array<double, max_Rec_types + 1>, max_Rec_types + 1> Lig_Rec_inter_matrix = {}; //init from input
	std::array<std::array<double, max_Rec_types + 1>, max_Rec_types + 1> exp_m_Lig_Rec_inter_matrix = {};

	void Check_Ligand_lists();
	int nr_Lig_types = 2; //init from input
	std::array<int, max_Rec_types + 1> N_lig_per_type_per_poly = {}; // [0]-> total n lig per poly, [type] n lig per type

	double total_bond_U = 0;
	double Calc_bond_energy();
	void Check_Bonds();
	void Check_Rec_Cell_List();


	std::array < std::array<double, max_blobs_in_poly>, max_Rec_types + 1> lig_in_poly_counts_insert{}; // for calculating the distribution of the numbers of ligads on inserted polymers


	uint64_t dist_sum_nr = 0;
	double dist_blob_blob_bond_sum = 0;
	uint64_t cos_theta_sum_nr = 0;
	double cos_theta_sum = 0;
	double theta_sum = 0;

	// blob-blob distance bins
	double max_dist_bin = 10;
	double dist_bin_width = max_dist_bin / (double)total_nr_dist_bins;

	std::array<double, total_nr_dist_bins> dist_blob_blob_bond_bins{};
	// blob-blob-blob cos(theta) bins
	double max_cos_theta_bin = 1;
	double start_cos_theta_bin = -1;
	double cos_theta_bin_width = (max_cos_theta_bin - start_cos_theta_bin) / (double)total_nr_cos_theta_bins;

	std::array<double, total_nr_cos_theta_bins> cos_theta_bins{};
	// blob-blob-blob cos(theta) bins
	double max_theta_bin = pi;
	double theta_bin_width = (max_theta_bin) / (double)total_nr_theta_bins;

	std::array<double, total_nr_theta_bins> theta_bins{};

	//g_r_rec_rec bins
	std::array<std::array< std::array<double, total_nr_g_r_rec_rec_bins>, max_Rec_types>, max_Rec_types> g_r_rec_rec{};
	bool calc_g_r_rec_rec = true;
	double max_g_r_rec_rec = rcut_Lig_Rec; // init from input
	std::array<double, max_Rec_types> count_g_r_rec_rec = {};
	double g_r_rec_rec_bin_width = max_g_r_rec_rec / total_nr_g_r_rec_rec_bins;


	double Calculate_receptor_LJ_interactions(REC current_trial_rec, int current_rec_id, bool count_g_rr);

	double Calculate_harmonic_bond_interactions_poly(POLYMER poly);
	double Calculate_bond_bending_interactions_poly(POLYMER poly);
	double Calculate_wall_interactions_blob(BLOB blob);

	BLOB Get_bond_vector(BLOB blob_0, BLOB blob_1);

	void Sample_perst_len(int poly_id);
	double sum_sum_l_dot_l = 0;
	double count_sum_l_dot_l = 0;
	
	void Sample_end_to_end_dist(int poly_id);
	double end_to_end_dist_sum = 0;
	double end_to_end_dist_2_sum = 0;
	double count_end_to_end_dist = 0;

	double Calc_Rg(int poly_id);

	void Sample_Rg(int poly_id);
	double Rg_sum = 0;
	double count_Rg_sum = 0;


	void Sample_Nr_Lig(int poly_id);
	std::array < std::array<double, max_blobs_in_poly>, max_Rec_types + 1> lig_in_poly_counts_in_sim{}; // for calculating the distribution of the numbers of ligads in simulation
	int count_lig_in_poly_sum = 0;





};

