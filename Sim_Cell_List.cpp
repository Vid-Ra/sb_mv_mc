#include "Simulation.h"





void Simulation::Make_Cell_List()
{
	N_cells[0] = (int)(box_x / rcut_Blob_Blob* cell_list_ideal_side_rcut_div);
	if (N_cells[0] > max_cells)
	{
		N_cells[0] = max_cells;
	}
	N_cells[1] = (int)(box_y / rcut_Blob_Blob* cell_list_ideal_side_rcut_div);
	if (N_cells[1] > max_cells)
	{
		N_cells[1] = max_cells;
	}	
	N_cells[2] = (int)(box_z / rcut_Blob_Blob* cell_list_ideal_side_rcut_div);
	if (N_cells[2] > max_cells)
	{
		N_cells[2] = max_cells;
	}

	cell_side[0] = box_x / N_cells[0];
	cell_side[1] = box_y / N_cells[1];
	cell_side[2] = box_z / N_cells[2];



	for (int i = 0; i < max_cells; i++)
	{
		for (int j = 0; j < max_cells; j++)
		{
			for (int k = 0; k < max_cells; k++)
			{
				cell_list_cell_xyz_N_cell[i][j][k] = 0;

				for (int  ii = 0; ii < max_blobs_in_cell; ii++)
				{
					cell_list_cell_xyz_ids[i][j][k][ii] = -1;
				}
			}
		}
	}

	for (int i = 0; i < max_total_blobs; i++)
	{
		

		cell_list_id_cell_xyz[i] = { -1 ,-1 ,-1 };

	}





	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{
		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{
			int blob_uid = poly_i * Blobs_in_poly + blob_i;

			int cell_x = (int)floor((Polymers[poly_i][blob_i][0] ) / cell_side[0]);
			int cell_y = (int)floor((Polymers[poly_i][blob_i][1] ) / cell_side[1]);
			int cell_z = (int)floor((Polymers[poly_i][blob_i][2] ) / cell_side[2]);


			if (cell_z < 0)
			{
				cell_z = 0;
			}
			else if (cell_z > N_cells[2]-1)
			{
				cell_z = N_cells[2]-1;
			}

			cell_list_id_cell_xyz[blob_uid] = { cell_x ,cell_y ,cell_z };

		}
	}

	for (int blob_uid = 0; blob_uid < nr_Poly* Blobs_in_poly; blob_uid++)
	{
		std::array<int, 3> blob_uid_cell_xyz = cell_list_id_cell_xyz[blob_uid];
		cell_list_cell_xyz_ids[blob_uid_cell_xyz[0]][blob_uid_cell_xyz[1]][blob_uid_cell_xyz[2]][cell_list_cell_xyz_N_cell[blob_uid_cell_xyz[0]][blob_uid_cell_xyz[1]][blob_uid_cell_xyz[2]]] = blob_uid;
		cell_list_cell_xyz_N_cell[blob_uid_cell_xyz[0]][blob_uid_cell_xyz[1]][blob_uid_cell_xyz[2]]++;
	}
}




void Simulation::Check_Cell_List()
{
	std::ofstream log_out;
	log_out.open("log.txt", std::ios_base::app);
	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{
		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{
			int blob_uid = poly_i * Blobs_in_poly + blob_i;

			int cell_x = (int)floor((Polymers[poly_i][blob_i][0] ) / cell_side[0]);
			int cell_y = (int)floor((Polymers[poly_i][blob_i][1] ) / cell_side[1]);
			int cell_z = (int)floor((Polymers[poly_i][blob_i][2] ) / cell_side[2]);


			if (cell_z < 0)
			{
				cell_z = 0;
			}
			else if (cell_z > N_cells[2]-1)
			{
				cell_z = N_cells[2]-1;
			}

			std::array<int, 3> cell_xyz = { cell_x, cell_y, cell_z };
			if (cell_list_id_cell_xyz[blob_uid] != cell_xyz)
			{
				log_out << "Error with cell list " << cell_xyz[0] << " " << cell_xyz[1] << " " << cell_xyz[2] << "   " << cell_list_id_cell_xyz[blob_uid][0] << " " << cell_list_id_cell_xyz[blob_uid][1] << " " << cell_list_id_cell_xyz[blob_uid][2] << "\n";
			}

			bool bl = true;
			for (int i = 0; i < cell_list_cell_xyz_N_cell[cell_x][cell_y][cell_z]; i++)
			{
				if (cell_list_cell_xyz_ids[cell_x][cell_y][cell_z][i] == blob_uid)
				{
					bl = false;
					break;
				}
			}
			if (bl)
			{
				log_out << "Error with cell list, no blob uid in cell_list_cell_xyz_ids " << cell_xyz[0] << " " << cell_xyz[1] << " " << cell_xyz[2] << "\n";

			}
		}
	}
	log_out.close();
}



