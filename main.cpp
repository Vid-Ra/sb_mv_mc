#include <iostream>
#include <fstream>
#include <time.h>
#include <iomanip> 
#include <signal.h>


#include "Blob.h"
#include "random.h"
#include "Consts.h"
#include "Simulation.h"

/*
* Soft Blob Multivalent Monte Carlo
* Input file named SB_MV_MC.inp
* 
* simulation runs in class Simulation (Simulation.cpp/h, Sim_*.cpp)
* Blobs and Polymers are stored in std::array-s defined as:
*	#define POLYMER std::array<std::array<double, 3>, max_blobs_in_poly>
*	#define BLOB std::array<double, 3>
*	
* Polymers are stored in array Polymers:  std::array<POLYMER, max_poly_in_sim> Polymers
* 
* Functions to do with Blobs are in Blob.cpp/h
* 
* Functions to do with Ligand Receptors in Sim_Lig_Rec.cpp, Binding takes place in MC_move_Blob combined with standard MC move
* 
* Cell lists:
* 
*	std::array<std::array<std::array<int, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_N_cell;
*		cell_list_cell_xyz_N_cell[x_cell][y_cell][z_cell] - > Number of blobs in cell
*	std::array<std::array<std::array<std::array<int, max_blobs_in_cell>, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_ids;
*		cell_list_cell_xyz_ids[x_cell][y_cell][z_cell] -> Array of Blob_Uids in cell (Blob_Uid = poly_id*Blobs_in_poly+blob_id)
*	std::array< std::array<int, 3>, max_total_blobs> cell_list_id_cell_xyz;
*		cell_list_id_cell_xyz[Blob_Uid] -> {x_cell,y_cell,z_cell} of blob 
*	
* Receptor Cell Lists
*  std::array<std::array<int, 2>, max_N_rec> Rec_Cell_List_rid_cell_xy;
*  std::array<std::array<std::array<int, max_blobs_in_cell>, max_cells>, max_cells> Rec_Cell_List_cell_xy_rid;
*  std::array<std::array<int, max_cells>, max_cells> Rec_Cell_List_cell_xy_N;
* 
* 
* 
* 
* Receptor, Ligand, Bond Bookeeping:
* 
* std::array<REC, max_N_rec> Receptors;
* std::array<std::array<int, 3>, max_N_rec> Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}
* std::array<int, max_poly_in_sim> Ligands_N_in_Poly;
* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_in_Poly;     //  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
* std::array<std::array<std::array<int, max_ligands_in_poly + 1>, max_blobs_in_poly>, max_poly_in_sim> Ligands_in_Blob;       // Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_Types_Bonds;       // Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}
* 
* 
*/


Simulation sim;



void signalHandler(int signum) {
    
    // handles signals
    extern Simulation sim;
    sim.exit_signal = true; // breaks main loop of simulation
    std::ofstream log_out;
    log_out.open("log.txt", std::ios_base::app);
    log_out << "Exiting, recieved signal " << signum << "\n";
    log_out.close();
    //exit(signum);  
}


int main()
{
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);



    sim =  Simulation("SB_MV_MC.inp");

	sim.Run_Sim();

	
	
	return 0;
}