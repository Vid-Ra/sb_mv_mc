#pragma once


const double R = 8.3144598; //[J/molK]
const double pi = 3.14159265359;
const double Na = 6.02214076e23;
const double k_b = R / Na;
const double e_0 = 1.60217662e-19;
const double eps_0 = 8.8541878128e-12;
const double u_atom = 1.66054E-27; //[kg]
const double h = 6.62618E-34; // [J/s]