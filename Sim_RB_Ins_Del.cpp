#include "Simulation.h"

extern std::mt19937 generator;



double Simulation::Calculate_Ext_Interactions_Blob(POLYMER trial_poly, BLOB trial_blob, int n_blobs_in_trial_poly, int old_poly_id) //old_poly_id negative - insertion
{

	/* 
	* Calculates external interactions (No Blob-Blob harmonic, bending)
	* Used in RB/conf. bias insert and delete
	*/
	double U_blob_blob = 0;
	double U_bending = 0;
	double U_wall = 0;

	//std::cout << "_______________ext_int_________________" << "\n";
	//std::cout << "n_blobs_in_trial_poly " << n_blobs_in_trial_poly <<" old_poly_id " << old_poly_id << "\n";
	
	

	int cell_x = (int)floor((trial_blob[0] ) / cell_side[0]); // calculate cell of trial_blob 
	int cell_y = (int)floor((trial_blob[1] ) / cell_side[1]);
	int cell_z = (int)floor((trial_blob[2] ) / cell_side[2]);


	if (cell_z < 0)
	{
		cell_z = 0;
	}
	else if (cell_z > N_cells[2] - 1)
	{
		cell_z = N_cells[2] - 1;
	}

	for (int k_cell = cell_z - cell_list_around; k_cell < cell_z + cell_list_around+1; k_cell++)
	{

		if (k_cell < 0 || k_cell >  N_cells[2] - 1)
		{
			continue;
		}

		for (int j_cell_tmp = cell_y - cell_list_around; j_cell_tmp < cell_y + cell_list_around+1; j_cell_tmp++)
		{

			int j_cell = j_cell_tmp;
			if (j_cell < 0)
			{
				j_cell += N_cells[1];
			}
			else if (j_cell > N_cells[1] - 1)
			{
				j_cell -= N_cells[1];
			}

			for (int i_cell_tmp = cell_x - cell_list_around; i_cell_tmp < cell_x + cell_list_around+1; i_cell_tmp++) // loops over cells +-1
			{
				//int i_cell_o = i_cell_tmp - N_cells[0] * (int)floor((double)(i_cell_tmp) / N_cells[0] + 1.0e-8); // PBC in x,y
				//int j_cell_o = j_cell_tmp - N_cells[1] * (int)floor((double)(j_cell_tmp) / N_cells[1] + 1.0e-8);
				
				
				int i_cell = i_cell_tmp;

				if (i_cell < 0)
				{
					i_cell += N_cells[0];
				}
				else if (i_cell > N_cells[0] - 1)
				{
					i_cell -= N_cells[0];
				}
				/*if (i_cell_o != i_cell || j_cell_o != j_cell)
				{
					std::cout << "\n";
				}*/

				for (int cell_blob_i = 0; cell_blob_i < cell_list_cell_xyz_N_cell[i_cell][j_cell][k_cell]; cell_blob_i++) // loop over blobs in specific cell
				{
					

					
					int blob_uid1 = cell_list_cell_xyz_ids[i_cell][j_cell][k_cell][cell_blob_i];
					int blob_i = blob_uid1 % Blobs_in_poly;
					int poly_i = (int)floor((double)blob_uid1 / (double)Blobs_in_poly);

					if (!(old_poly_id == poly_i)) // does not count Polymer with old_poly_id (-1 for insertion)
					{


						double dist_2 = dist_2_Blob_min_img_x_y(trial_blob, Polymers[poly_i][blob_i], box_x, box_x_2, box_y, box_y_2);
						
						if (dist_2 < rcut_Blob_Blob_2)
						{
							/*
							if (temp_bool)
							{
								std::cout << "cell_list:  " << i_cell << " " << j_cell << " " << k_cell << " " << dist << " " << poly_i << " " << blob_i << "\n";
							}*/
							U_blob_blob += interaction_r2_Blob_Blob(dist_2) - pot_shift_Blob_Blob;
						}
					}
				}
			}
		}
	}
	
	

	//std::cout << "U_blob_blob  " << U_blob_blob << "\n";

	for (int poly_i = 0; poly_i < n_blobs_in_trial_poly; poly_i++) // BLOB-BLOB gaussinan interaction with blobs in trial polymer, only up to n_blobs_in_trial_poly
	{
		double dist_2 = dist_2_Blob_min_img_x_y(trial_blob, trial_poly[poly_i], box_x, box_x_2, box_y, box_y_2);

		if (dist_2 < rcut_Blob_Blob_2)
		{
			/*
			if (temp_bool)
			{
				std::cout << "cell_list:  " << i_cell << " " << j_cell << " " << k_cell << " " << dist << " " << poly_i << " " << blob_i << "\n";
			}*/
			U_blob_blob += interaction_r2_Blob_Blob(dist_2) - pot_shift_Blob_Blob;
		}
	}


	/*
	// bending angle potential for the blob one blob back
	if (n_blobs_in_trial_poly > 1)
	{
		BLOB blob_m2 = trial_poly[n_blobs_in_trial_poly - 2];
		BLOB blob_m1 = trial_poly[n_blobs_in_trial_poly - 1];
		double cos_theta = calc_cos_bond_angle(blob_m2, blob_m1, trial_blob);
		U_bending = interaction_cos_theta_bending(eps_b, cos_theta);
	}
	*/

	//std::cout << "U_blob_blob1 "<< U_blob_blob << "\n";


	U_wall = Calculate_wall_interactions_blob(trial_blob);

	//std::cout << "U_bend EXT " << U_bending << "\n";
	//U_bend_ext_sum += U_bending;
	return U_blob_blob + U_wall;//+ U_bending;
}




//double U_Trials[max_k]{};
//double exp_U_Trials[max_k]{};
//BLOB Trial_Blobs[max_k]{};
std::array<double, max_k>	 U_Trials{};
std::array<double, max_k>	 exp_U_Trials{};
std::array<BLOB, max_k>	 Trial_Blobs{};


//int temp_blob_list[max_ligands_in_poly]{};
std::array<int, max_ligands_in_poly>	 temp_blob_list{};


void Assign_uniform_block_copoly_2_type_same_nr(int blobs_in_poly, int total_lig_in_poly,bool do_block_copoly_indent)
{
	// uniform block copolymer 1x2x1x2x1x2x1x2x1x2x
	if (total_lig_in_poly == 0) return;

	for (int i = 0; i < blobs_in_poly; i++)
	{
		temp_blob_list[i] = 0;

	}
	//int types_list[3]{};
	std::array<int, 3>	 types_list{};

	double ran = RandomDouble01();
	if (ran <= 0.5)
	{
		types_list[0] = 0;
		types_list[1] = 1;
		types_list[2] = 2;
	}
	else
	{
		types_list[0] = 0;
		types_list[1] = 2;
		types_list[2] = 1;
	}

	int nr_no_lig = blobs_in_poly - total_lig_in_poly;


	int no_lig_after_lig = floor(nr_no_lig / total_lig_in_poly); // number of blobs without ligands after a blob with ligands

	int c_lig_typ = 0;

	int total_block = (1 + no_lig_after_lig) * total_lig_in_poly;

	int leftover = blobs_in_poly - total_block;
	int indent = 0;
	if (do_block_copoly_indent)
	{
		indent = floor(leftover / 2 + no_lig_after_lig / 2);
	}
	 
	
	for (int i = 0; i < indent; i++)
	{
		temp_blob_list[i] = types_list[0];
	}
	for (int i = 0; i < blobs_in_poly - indent; i++)
	{
		if (i % (1 + no_lig_after_lig) == 0 && i <= total_block - 1) // ligand
		{
			if (c_lig_typ == 1)
			{
				temp_blob_list[indent+i] = types_list[1];
				c_lig_typ = 0;
			}

			else
			{
				temp_blob_list[indent+i] = types_list[2];
				c_lig_typ++;
			}
		}
		else
		{
			temp_blob_list[indent+i] = types_list[0];

		}
	}



}



void Assign_centre_block_copoly_2_types(int blobs_in_poly, int Nlig_1, int Nlig_2 )
{
	// centre block copolymer - xxxxxx12121212xxxxxx
	// different numbers of ligands supported
	int	total_lig_in_poly = Nlig_1 + Nlig_2;

	
	if (total_lig_in_poly == 0) return;

	for (int i = 0; i < blobs_in_poly; i++)
	{
		temp_blob_list[i] = 0;

	}

	int less_Nlig_typ_ls[2] = { 1, 2 };
	int less_Nlig_ls[2] = { Nlig_1, Nlig_2 };
	if (Nlig_2 < Nlig_1)
	{
		less_Nlig_typ_ls[0] = 2;
		less_Nlig_typ_ls[1] = 1;
		less_Nlig_ls[0] = Nlig_2;
		less_Nlig_ls[1] =  Nlig_1;

	}

	if (Nlig_2 == Nlig_1)
	{
		double ran = RandomDouble01();
		if (ran <= 0.5)
		{
			less_Nlig_typ_ls[0] = 2;
			less_Nlig_typ_ls[1] = 1;
			less_Nlig_ls[0] = Nlig_2;
			less_Nlig_ls[1] = Nlig_1;
		}
	}



	int leftover = blobs_in_poly - total_lig_in_poly;
	int indent = 0;

	indent = floor(leftover / 2);
	int nr_lig_after_lig = 0;
	if (less_Nlig_ls[0] != 0)
	{
		nr_lig_after_lig = floor(less_Nlig_ls[1] / less_Nlig_ls[0]);
	}
	int block_size = nr_lig_after_lig + 1;
	int total_block = block_size * less_Nlig_ls[0];
	int N_blocks = int(total_block / block_size);

	int leftover_lig_more = blobs_in_poly - leftover - total_block;
	int indent_lig_more = ceil(leftover_lig_more / 2);


	int blob_i = 0;

	for (int i = 0; i < indent; i++)
	{
		temp_blob_list[blob_i] = 0;
		blob_i++; 
	}

	for (int i = 0; i < indent_lig_more; i++)
	{
		temp_blob_list[blob_i] = less_Nlig_typ_ls[1];
		blob_i++;
	}

	for (int i = 0; i < N_blocks; i++)
	{
		for (int j = 0; j < block_size; j++)
		{
			if (j == 0)
				temp_blob_list[blob_i] = less_Nlig_typ_ls[0];
			else
				temp_blob_list[blob_i] = less_Nlig_typ_ls[1];



			blob_i++;
		}
	}

	for (int i = 0; i < leftover_lig_more - indent_lig_more; i++)
	{
		temp_blob_list[blob_i] = less_Nlig_typ_ls[1];
		blob_i++;
	}


	for (int i = 0; i < leftover - indent; i++)
	{

		temp_blob_list[blob_i] = 0;
		blob_i++;


	}



}






void Assign_centre_block_copoly_blocks_2_types(int blobs_in_poly, int Nlig_1, int Nlig_2)
{
	// centre block copolymer blocks of ligands - xxxxxx11112222xxxxxx
	// different numbers of ligands supported
	int	total_lig_in_poly = Nlig_1 + Nlig_2;


	if (total_lig_in_poly == 0) return;

	for (int i = 0; i < blobs_in_poly; i++)
	{
		temp_blob_list[i] = 0;

	}

	int less_Nlig_typ_ls[2] = { 1, 2 };
	int less_Nlig_ls[2] = { Nlig_1, Nlig_2 };
	if (Nlig_2 < Nlig_1)
	{
		less_Nlig_typ_ls[0] = 2;
		less_Nlig_typ_ls[1] = 1;
		less_Nlig_ls[0] = Nlig_2;
		less_Nlig_ls[1] = Nlig_1;

	}

	if (Nlig_2 == Nlig_1)
	{
		double ran = RandomDouble01();
		if (ran <= 0.5)
		{
			less_Nlig_typ_ls[0] = 2;
			less_Nlig_typ_ls[1] = 1;
			less_Nlig_ls[0] = Nlig_2;
			less_Nlig_ls[1] = Nlig_1;
		}
	}



	int leftover = blobs_in_poly - total_lig_in_poly;
	int indent = 0;

	indent = floor(leftover / 2);
	int nr_lig_after_lig = 0;
	if (less_Nlig_ls[0] != 0)
	{
		nr_lig_after_lig = floor(less_Nlig_ls[1] / less_Nlig_ls[0]);
	}
	//int block_size = nr_lig_after_lig + 1;
	//int total_block = block_size * less_Nlig_ls[0];
	//int N_blocks = int(total_block / block_size);

	int leftover_lig_more = blobs_in_poly - leftover - Nlig_1 - Nlig_2;
	int indent_lig_more = ceil(leftover_lig_more / 2);


	int blob_i = 0;

	for (int i = 0; i < indent; i++)
	{
		temp_blob_list[blob_i] = 0;
		blob_i++;
	}

	for (int i = 0; i < indent_lig_more; i++)
	{
		temp_blob_list[blob_i] = less_Nlig_typ_ls[1];
		blob_i++;
	}
	/*
	for (int i = 0; i < N_blocks; i++)
	{
		for (int j = 0; j < block_size; j++)
		{
			if (j == 0)
				temp_blob_list[blob_i] = less_Nlig_typ_ls[0];
			else
				temp_blob_list[blob_i] = less_Nlig_typ_ls[1];



			blob_i++;
		}
	}
	*/
	for (int i = 0; i < less_Nlig_ls[0]; i++)
	{
		temp_blob_list[blob_i] = less_Nlig_typ_ls[0];
		blob_i++;
	}

	for (int i = 0; i < less_Nlig_ls[1]; i++)
	{
		temp_blob_list[blob_i] = less_Nlig_typ_ls[1];
		blob_i++;
	}

	for (int i = 0; i < leftover_lig_more - indent_lig_more; i++)
	{
		temp_blob_list[blob_i] = less_Nlig_typ_ls[1];
		blob_i++;
	}


	for (int i = 0; i < leftover - indent; i++)
	{
		temp_blob_list[blob_i] = 0;
		blob_i++;
	}
}










void Simulation::RB_Insert()
{
	/* 
	* Rosenblut/conf. bias insertion of a Polymer
	* 
	*/
	POLYMER trial_poly{};

	// first blob:
	//std::cout << "____________________________INSERT____________________________" << "\n\n";

	BLOB trial_blob = { RandomDouble01() * box_x, RandomDouble01() * box_y, RandomDouble01() * box_z }; //first blob randomly in box

	int old_poly_id = -1; // insertion, so no old poly

	double U_ext = 0;


	U_ext = Calculate_Ext_Interactions_Blob(trial_poly, trial_blob, 0, old_poly_id);

	trial_poly[0] = trial_blob;

	double W_chain = exp(-U_ext);// Rosenbluth factor
	//double chosen_U_ext_bolb[max_blobs_in_poly]{};

	std::array<double, max_blobs_in_poly>	 chosen_U_ext_bolb{};

	double sum_exp_U = 0;


	for (int n_blob = 1; n_blob < Blobs_in_poly; n_blob++)
	{

		
		sum_exp_U = 0;
		
		for (int trial_nr = 0; trial_nr < k; trial_nr++) // k trials
		{


			if (n_blob == 1) // second blob, no angle potential yet
			{
				trial_blob = Random_Blob_close_sphere(trial_poly[n_blob - 1], box_x, box_y);
			}
			else // other blobs, the distribution also biased by the bending potential
			{

				trial_blob = Random_Blob_close_sphere_theta_bias(trial_poly[n_blob - 1], trial_poly[n_blob - 2], box_x, box_y, max_P_theta, eps_b);

			}


			U_Trials[trial_nr] = Calculate_Ext_Interactions_Blob(trial_poly, trial_blob, n_blob, old_poly_id);

			
			Trial_Blobs[trial_nr] = trial_blob;
			exp_U_Trials[trial_nr] = exp(-U_Trials[trial_nr]);
			sum_exp_U += exp_U_Trials[trial_nr];

		}
		W_chain *= sum_exp_U/k; // Rosenbluth factor
		// pick a trial blob with probability prop to exp(-U_trial)

		double ran = RandomDouble01();
		double sum_norm_exp_U_trials = 0;
		int chosen_trial = -1;
		if (sum_exp_U != 0) 
		{
			for (int trial_nr = 0; trial_nr < k; trial_nr++) // normalize exp_U_Trials
			{
				double norm_exp_U_Trials = exp_U_Trials[trial_nr] / sum_exp_U;


				sum_norm_exp_U_trials += norm_exp_U_Trials;
				if (sum_norm_exp_U_trials > ran) // choose trial with normalized exp_U_Trials
				{
					chosen_trial = trial_nr;
					break;
				}
			}

		}
		else // very stiff polymer going thorugh the box wall, all eneregies exp(u_ext) = 0
		{
			chosen_trial = RandomInt(k); // just pick a random trial, does not matter, will not be ever accepted anyways
		}
		
		trial_poly[n_blob] = Trial_Blobs[chosen_trial];
		chosen_U_ext_bolb[n_blob] = U_Trials[chosen_trial];

	}

	//acceptence criterion


	/*
	       !metropolis criterion
        Q=1
        arg=activity*volume/(nchains+1)*Wchain*Q
	*/
	double volume = box_x * box_y * box_z;
	double Q = 1;

	double arg = exp_mu * volume / (nr_Poly + 1) * W_chain * Q;
	double ran = RandomDouble01();
	/*if (temp_bool)
	{
		arg = 1;
	}*/
	//arg = 1; ///////////////////////////////////////////////////////
	if (ran < arg)
	{
		//double U_old_t = Total_Energy();

		
		Polymers[nr_Poly] = trial_poly;

		nr_Poly++;
		double dU = U_ext; //energy of first one

		for (int blob_i = 1; blob_i < Blobs_in_poly; blob_i++)
		{
			dU += chosen_U_ext_bolb[blob_i]; //energies of the chosen ones, plus bond energies, plus bending energies
			dU += interaction_Blob_Blob_bond(dist_Blob_min_img_x_y(trial_poly[blob_i],trial_poly[blob_i - 1], box_x, box_x_2, box_y, box_y_2));
			if (blob_i != 1)
			{
				BLOB blob_m2 = trial_poly[blob_i - 2];
				BLOB blob_m1 = trial_poly[blob_i - 1];
				double cos_theta = calc_cos_bond_angle(blob_m2, blob_m1, trial_poly[blob_i]);
				double U_bending = interaction_cos_theta_bending(eps_b, cos_theta);
				dU += U_bending;
			}

		}

		total_U += dU;


		// update cell lists

		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i ++)
		{
			int cell_uid = (nr_Poly - 1) * Blobs_in_poly + blob_i;
			int x_cell_n = (int)floor((trial_poly[blob_i][0] ) / cell_side[0]);
			int y_cell_n = (int)floor((trial_poly[blob_i][1] ) / cell_side[1]);
			int z_cell_n = (int)floor((trial_poly[blob_i][2] ) / cell_side[2]);

			if (z_cell_n < 0)
			{
				z_cell_n = 0;
			}
			else if (z_cell_n > N_cells[2] - 1)
			{
				z_cell_n = N_cells[2] - 1;
			}

			std::array<int, 3> xyz_cell_n = { x_cell_n ,y_cell_n ,z_cell_n };


			cell_list_id_cell_xyz[cell_uid] = xyz_cell_n;


			int N_new_cell = cell_list_cell_xyz_N_cell[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]];

			cell_list_cell_xyz_ids[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]][N_new_cell] = cell_uid;


			cell_list_cell_xyz_N_cell[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]]++;
		}



		// insert ligands

		// only one ligand per blob for now...
		if (! block_copoly)
		{	
			if (!poiss_ligand_dist)
			{
				// asign blobs to ligands randomly
				for (int i = 0; i < Blobs_in_poly; i++)
				{
					temp_blob_list[i] = i;
				}

				int i_ligand = 0;
				for (int lig_type = 1; lig_type < nr_Lig_types + 1; lig_type++)
				{
					for (int i = 0; i < N_lig_per_type_per_poly[lig_type]; i++)
					{

						int ran_blob_i = (int)floor(RandomDouble01() * (Blobs_in_poly - i_ligand)) + i_ligand; // minimum value of i_ligand, below are already saved
						int temp_blob_i_save = temp_blob_list[i_ligand];
						temp_blob_list[i_ligand] = temp_blob_list[ran_blob_i]; // swap i_ligand and ran_blob_i
						temp_blob_list[ran_blob_i] = temp_blob_i_save;

						int blob_i = temp_blob_list[i_ligand]; // blob on which receptor is located


						Ligands_in_Blob[nr_Poly - 1][blob_i][0]++;
						int Lig_in_Bl = Ligands_in_Blob[nr_Poly - 1][blob_i][0];
						Ligands_in_Blob[nr_Poly - 1][blob_i][Lig_in_Bl] = i_ligand;

						Ligands_in_Poly[nr_Poly - 1][i_ligand] = { blob_i ,Lig_in_Bl };
						Ligands_N_in_Poly[nr_Poly - 1]++;

						Ligands_Types_Bonds[nr_Poly - 1][i_ligand] = { lig_type ,-1 };

						i_ligand++;
					}
				}
			}
			else
			{
				// Poisson distribution of blobs
				std::array<int, max_Rec_types + 1> N_lig_per_type_per_poly_actuall{};
				std::array<std::poisson_distribution<int>, max_Rec_types+1> poiss_dist_array;
				for (int i = 1; i < nr_Lig_types+1; i++)
				{
					std::poisson_distribution<int> p_dist(N_lig_per_type_per_poly[i]);
					poiss_dist_array[i] = p_dist;
				}

				int total_lig_in_poly = Blobs_in_poly + 1;
				while (total_lig_in_poly > Blobs_in_poly)
				{
					total_lig_in_poly = 0;
					for (int i = 1; i < nr_Lig_types + 1; i++)
					{
						int poiss_n_lig = poiss_dist_array[i](generator);
						N_lig_per_type_per_poly_actuall[i] = poiss_n_lig;
						total_lig_in_poly += poiss_n_lig;
					}
				}

				for (int i = 1; i < nr_Lig_types + 1; i++)
				{
					lig_in_poly_counts_insert[i][N_lig_per_type_per_poly_actuall[i]] += 1;
				}
				

				// asign blobs to ligands randomly with the poisson distrbuted numbers of ligands
				for (int i = 0; i < Blobs_in_poly; i++)
				{
					temp_blob_list[i] = i;
				}

				int i_ligand = 0;
				for (int lig_type = 1; lig_type < nr_Lig_types + 1; lig_type++)
				{
					for (int i = 0; i < N_lig_per_type_per_poly_actuall[lig_type]; i++)
					{

						int ran_blob_i = (int)floor(RandomDouble01() * (Blobs_in_poly - i_ligand)) + i_ligand; // minimum value of i_ligand, below are already saved
						int temp_blob_i_save = temp_blob_list[i_ligand];
						temp_blob_list[i_ligand] = temp_blob_list[ran_blob_i]; // swap i_ligand and ran_blob_i
						temp_blob_list[ran_blob_i] = temp_blob_i_save;

						int blob_i = temp_blob_list[i_ligand]; // blob on which receptor is located


						Ligands_in_Blob[nr_Poly - 1][blob_i][0]++;
						int Lig_in_Bl = Ligands_in_Blob[nr_Poly - 1][blob_i][0];
						Ligands_in_Blob[nr_Poly - 1][blob_i][Lig_in_Bl] = i_ligand;

						Ligands_in_Poly[nr_Poly - 1][i_ligand] = { blob_i ,Lig_in_Bl };
						Ligands_N_in_Poly[nr_Poly - 1]++;

						Ligands_Types_Bonds[nr_Poly - 1][i_ligand] = { lig_type ,-1 };

						i_ligand++;
					}
				}


			}
			
		}
		else // block copolimer, currently works for 2 lig types, same nr. of ligands
		{
			//Assign_centre_block_copoly_2_type_same_nr(Blobs_in_poly, N_lig_per_type_per_poly[1] + N_lig_per_type_per_poly[2]);
			if (nr_Lig_types != 2 )
			{
				std::cout << "nr_Lig_types != 2 " << "\n";
				exit(3);
			}
			if (block_copoly == 1) // uniform block copolimer
			{
				if (N_lig_per_type_per_poly[1] != N_lig_per_type_per_poly[2])
				{
					std::cout << "N_lig_per_type_per_poly[1] != N_lig_per_type_per_poly[2] " << "\n";
					exit(3);
				}
				Assign_uniform_block_copoly_2_type_same_nr(Blobs_in_poly, N_lig_per_type_per_poly[1] + N_lig_per_type_per_poly[2], do_block_copoly_indent);
			}
			else  if (block_copoly == 2)// center block copolymer
				Assign_centre_block_copoly_2_types(Blobs_in_poly, N_lig_per_type_per_poly[1], N_lig_per_type_per_poly[2]);
			else
				Assign_centre_block_copoly_blocks_2_types(Blobs_in_poly, N_lig_per_type_per_poly[1], N_lig_per_type_per_poly[2]);

			int i_ligand = 0;
			for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
			{
				int lig_type = temp_blob_list[blob_i];
				if (lig_type != 0)
				{
					Ligands_in_Blob[nr_Poly - 1][blob_i][0]++;
					int Lig_in_Bl = Ligands_in_Blob[nr_Poly - 1][blob_i][0];
					Ligands_in_Blob[nr_Poly - 1][blob_i][Lig_in_Bl] = i_ligand;

					Ligands_in_Poly[nr_Poly - 1][i_ligand] = { blob_i ,Lig_in_Bl };
					Ligands_N_in_Poly[nr_Poly - 1]++;

					Ligands_Types_Bonds[nr_Poly - 1][i_ligand] = { lig_type ,-1 };

					i_ligand++;
				}

			}
		}
		
		

		//Check_Ligand_lists();
		//double U_new_t = Total_Energy();
		
		nr_acc_MC_RB_ins++;
		nr_acc_MC_RB_ins_cycle++;

	}


}

















void Simulation::RB_Delete()
{
	/*
	* Rosenblut/conf. bias deletion of a Polymer
	*
	*/
	int ran_poly_id = RandomInt(nr_Poly); //pick random polymer

	

	for (int lig_i = 0; lig_i < Ligands_N_in_Poly[ran_poly_id]; lig_i++)
	{
		if (Ligands_Types_Bonds[ran_poly_id][lig_i][1] != -1)
		{
			return; // polymer bonded
		}
	}



	POLYMER trial_poly = Polymers[ran_poly_id];

	// first blob:
	BLOB trial_blob = trial_poly[0]; // first blob from old poly,			 randomly first/last??

	int old_poly_id = ran_poly_id; // deletion, so old poly

	double U_ext = 0;
	//std::cout << "____________________________DELETE____________________________" << "\n\n";


	U_ext = Calculate_Ext_Interactions_Blob(trial_poly, trial_blob, 0, old_poly_id);

	//trial_poly.Blobs[0] = trial_blob;

	double W_chain = exp(-U_ext); // Rosenbluth factor
	//double chosen_U_ext_bolb[max_blobs_in_poly]{};
	std::array<double, max_blobs_in_poly>	 chosen_U_ext_bolb{};

	for (int n_blob = 1; n_blob < Blobs_in_poly; n_blob++)
	{


		double sum_exp_U = 0;

		int trial_nr = 0; 
		trial_blob = trial_poly[n_blob];  // first trial is existing poly
		U_Trials[trial_nr] = Calculate_Ext_Interactions_Blob(trial_poly, trial_blob, n_blob, old_poly_id);
		Trial_Blobs[trial_nr] = trial_blob;
		exp_U_Trials[trial_nr] = exp(-U_Trials[trial_nr]);
		sum_exp_U += exp_U_Trials[trial_nr];


		for (int trial_nr = 1; trial_nr < k; trial_nr++) // 1 to k trials
		{
			if (n_blob == 1) // second blob, no angle potential yet
			{
				trial_blob = Random_Blob_close_sphere(trial_poly[n_blob - 1], box_x, box_y);
			}
			else // other blobs, the distribution also biased by the bending potential
			{
				trial_blob = Random_Blob_close_sphere_theta_bias(trial_poly[n_blob - 1], trial_poly[n_blob - 2], box_x, box_y, max_P_theta, eps_b);

			}
			U_Trials[trial_nr] = Calculate_Ext_Interactions_Blob(trial_poly, trial_blob, n_blob, old_poly_id);
			Trial_Blobs[trial_nr] = trial_blob;
			exp_U_Trials[trial_nr] = exp(-U_Trials[trial_nr]);
			sum_exp_U += exp_U_Trials[trial_nr];
		}
		W_chain *= sum_exp_U / k; // Rosenbluth factor



		// pick original poly, -> first trial
		int chosen_trial = 0;

		chosen_U_ext_bolb[n_blob] = U_Trials[chosen_trial]; 

	}


	//acceptence criterion


	/*
		   !metropolis criterion
		Q=1
		arg=activity*volume/(nchains+1)*Wchain*Q
	*/
	double volume = box_x * box_y * box_z;
	double Q = 1;

	double arg = (nr_Poly) / (exp_mu * volume * W_chain * Q);
	double ran = RandomDouble01();
	/*
	if (temp_bool)
	{
		arg = 1;
	}*/
	//std::cout << arg << " " << W_chain << "\n";
	if (ran < arg)
	{
		//double U_old_t = Total_Energy();

		Polymers[ran_poly_id] = Polymers[nr_Poly-1];
		POLYMER empty_poly{};
		Polymers[nr_Poly - 1] = empty_poly;
		nr_Poly--;
		//std::cout << "del###" << nr_Poly << "\n";

		double dU = U_ext; //energy of first one
		for (int blob_i = 1; blob_i < Blobs_in_poly; blob_i++)
		{
			dU += chosen_U_ext_bolb[blob_i]; //energies of the chosen ones, plus bond energies
			dU += interaction_Blob_Blob_bond(dist_Blob_min_img_x_y(trial_poly[blob_i],trial_poly[blob_i - 1], box_x, box_x_2, box_y, box_y_2));
			if (blob_i != 1)
			{
				BLOB blob_m2 = trial_poly[blob_i - 2];
				BLOB blob_m1 = trial_poly[blob_i - 1];
				double cos_theta = calc_cos_bond_angle(blob_m2, blob_m1, trial_poly[blob_i]);
				dU += interaction_cos_theta_bending(eps_b, cos_theta);
			}
		}

		total_U -= dU;


		// update cell list
		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{
			int cell_uid = ran_poly_id * Blobs_in_poly + blob_i;
			int cell_uid_last = (nr_Poly)*Blobs_in_poly + blob_i; // overwrite ran_poly_id with the last one, delete last

			std::array<int, 3> xyz_cell_o = cell_list_id_cell_xyz[cell_uid];
			std::array<int, 3> xyz_cell_last = cell_list_id_cell_xyz[cell_uid_last];

			cell_list_id_cell_xyz[cell_uid] = xyz_cell_last;

			int N_old_cell = cell_list_cell_xyz_N_cell[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]];

			for (int c_uidi = 0; c_uidi < N_old_cell; c_uidi++) // deletes current polymer
			{
				if (cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][c_uidi] == cell_uid)
				{
					cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][c_uidi] = cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][N_old_cell - 1];
					cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][N_old_cell - 1] = 0;
					break;
				}
			}

			if (cell_uid != cell_uid_last) //replace last polymer blob uids with the swapped one - renames the last polymer into the current one
			{
				cell_list_id_cell_xyz[cell_uid_last] = { 0,0,0 };

				int N_last_cell = cell_list_cell_xyz_N_cell[xyz_cell_last[0]][xyz_cell_last[1]][xyz_cell_last[2]];

				for (int c_uidi = 0; c_uidi < N_last_cell; c_uidi++) 
				{
					if (cell_list_cell_xyz_ids[xyz_cell_last[0]][xyz_cell_last[1]][xyz_cell_last[2]][c_uidi] == cell_uid_last)
					{
						cell_list_cell_xyz_ids[xyz_cell_last[0]][xyz_cell_last[1]][xyz_cell_last[2]][c_uidi] = cell_uid;
						break;
					}
				}
			}
				

			cell_list_cell_xyz_N_cell[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]]--;
		}
			

		

		// update ligand lists
		
		Ligands_in_Poly[ran_poly_id] = Ligands_in_Poly[nr_Poly];
		Ligands_N_in_Poly[ran_poly_id] = Ligands_N_in_Poly[nr_Poly];
		Ligands_in_Blob[ran_poly_id] = Ligands_in_Blob[nr_Poly]; // move last poly into ran_poly_id


		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{
			Ligands_in_Blob[nr_Poly][blob_i][0] = 0;
			for (int lig_i_in_blob = 1; lig_i_in_blob < Ligands_in_Blob[ran_poly_id][blob_i][0]+1; lig_i_in_blob++)
			{
				Ligands_in_Blob[nr_Poly][blob_i][lig_i_in_blob] = -1;
			}
			
		}

		for (int lig_i = 0; lig_i < Ligands_N_in_Poly[nr_Poly]; lig_i++)
		{
			Ligands_in_Poly[nr_Poly][lig_i] = { -1,-1 };
		}

		// update bonds
		Ligands_Types_Bonds[ran_poly_id] = Ligands_Types_Bonds[nr_Poly];

		for (int lig_i = 0; lig_i < Ligands_N_in_Poly[nr_Poly]; lig_i++)
		{
			Ligands_Types_Bonds[nr_Poly][lig_i] = { 0,-1 };
		}

		for (int lig_id = 0; lig_id < Ligands_N_in_Poly[nr_Poly]; lig_id++)
		{
			int lig_Rec_bond = Ligands_Types_Bonds[ran_poly_id][lig_id][1];
			if (lig_Rec_bond != -1) // bond present
			{
				Receptor_Types_Bonds[lig_Rec_bond][1] = ran_poly_id;// swap nr_Poly for ran_poly_id in Receptor_Types_Bonds
				lig_Rec_bond = 0;
			}
		}

		Ligands_N_in_Poly[nr_Poly] = 0;

		// update N_bonds_in_chain
		N_bonds_in_chain[ran_poly_id] = N_bonds_in_chain[nr_Poly];
		N_bonds_in_chain[nr_Poly] = 0;





		//Check_Ligand_lists();
		//Check_Bonds();
		//double U_new_t = Total_Energy();

		nr_acc_MC_RB_del++;
		nr_acc_MC_RB_del_cycle++;


	}
}


void Simulation::RB_Ins_Del_MC_move()
{
	int ran = RandomInt(2);
	if (ran == 1) //insertion
	{
		if (nr_Poly < max_poly_in_sim)
		{
			RB_Insert();
		}
		nr_tot_MC_RB_ins++;
		nr_tot_MC_RB_ins_cycle++;

	}
	else
	{
		if (nr_Poly > 0) // deletion
		{
			RB_Delete();

		}
		nr_tot_MC_RB_del++;
		nr_tot_MC_RB_del_cycle++;

	}
		
}
