#include "Simulation.h"

std::mt19937 generator;

Simulation::Simulation()
{

}

Simulation::Simulation(std::string filename)
{
	//get input paremetrs from filename
	std::ifstream input;
	std::string tmp_str = "";

	input.open(filename);



	/*
	* ------------Soft Blob Multvalent Monte Carlo Input----------
	* -------------Simulation Lenght parameters-------------------
		total_nr_cycles
		out_frac_cycles
		ekv_frac
		init_from_file
		output_last_conf more_outputs
	*/
	std::getline(input,tmp_str);
	std::getline(input, tmp_str);
	input >> total_nr_cycles;
	input >> out_frac_cycles;
	input >> ekv_frac;
	bool init_from_file;
	input >> init_from_file;
	input >> output_last_conf;
	input >> more_outputs;
	ekv_cycles = (uint64_t)(total_nr_cycles * ekv_frac + 0.5);
	/*------------Simulation parameters--------------------------
		box_x y z
		mu
		eps_b
		k
		max_move_blob
		frac_ins_del_moves frac_rec_moves
		max_move_rec
		random_seed
	*/
	std::getline(input, tmp_str);
	if (tmp_str == "\r" || tmp_str == "")
	{
		std::getline(input, tmp_str); // second one needed becaues of "\r", maybe problem on linux written input????
	}

	input >> box_x >> box_y >> box_z;
	input >> mu;
	input >> eps_b;

	max_P_theta = calc_max_P_theta(eps_b);

	input >> k;
	input >> max_move_blob;
	input >> frac_ins_del_moves;
	input >> frac_rec_moves;
	input >> max_move_rec;
	input >> random_seed;
	if (random_seed <= 0)
	{
		random_seed = (int)time(NULL);
	}
	generator.seed(random_seed);

	/*
	----------Polymer, Ligand and Receptor Parameters----------
		n_lig_type n_rec_types
		Blobs_in_poly
		n lig of type in poly ...
		block_copoly poiss_ligand_dist
		n rec of type in box ...
		rcut_Lig_Rec
		Rec_HS_rad
	*/
	std::getline(input, tmp_str);
	if (tmp_str == "\r" || tmp_str == "")
	{
		std::getline(input, tmp_str); // second one needed becaues of "\r", maybe problem on linux written input????
	}

	input >> nr_Lig_types >> nr_Rec_types ;
	input >> Blobs_in_poly;
	int c_lig_in_poly = 0; //Numbers of Ligand types in polymer
	for (int i = 1; i < nr_Lig_types+1; i++)
	{
		input >> N_lig_per_type_per_poly[i];
		c_lig_in_poly += N_lig_per_type_per_poly[i];
	}
	N_lig_per_type_per_poly[0] = c_lig_in_poly;
	
	input >> block_copoly;
	input >> poiss_ligand_dist;

	int c_rec_type = 0; // numbers of Receptors per type
	for (int i = 1; i < nr_Lig_types + 1; i++)
	{
		input >> N_rec_per_type[i];
		c_rec_type += N_rec_per_type[i];
	}
	N_rec_per_type[0] = c_rec_type;
	total_N_Rec = c_rec_type;
	
	input >> rcut_Lig_Rec;
	rcut_Lig_Rec_2 = rcut_Lig_Rec * rcut_Lig_Rec;
	max_g_r_rec_rec = rcut_Lig_Rec;
	g_r_rec_rec_bin_width = max_g_r_rec_rec / total_nr_g_r_rec_rec_bins;
	//pot_shift_U_tether = interaction_r2_Bond_Tether(rcut_Lig_Rec_2);
	input >> Rec_HS_rad;
	Rec_HS_overlap_2 = 4 * Rec_HS_rad * Rec_HS_rad;

	/*
	------- Lig-Rec interaction matrix
	e11 e12 ...
	e21 e22 
	...     ...
	-----------
	*/
	std::getline(input, tmp_str);
	if (tmp_str == "\r" || tmp_str == "")
	{
		std::getline(input, tmp_str); // second one needed becaues of "\r", maybe problem on linux written input????
	}

	for (int i = 1; i < nr_Lig_types + 1; i++)
	{
		for (int j = 1; j < nr_Rec_types+1; j++)
		{
			input >> Lig_Rec_inter_matrix[i][j];
			exp_m_Lig_Rec_inter_matrix[i][j] = exp(-Lig_Rec_inter_matrix[i][j]);
		}
	}

	std::getline(input, tmp_str);
	if (tmp_str == "\r" || tmp_str == "")
	{
		std::getline(input, tmp_str); // second one needed becaues of "\r", maybe problem on linux written input????
	}

	/*
	----------------Rec - Rec LJ interaction matrix---------------- -
	do_Rec_LJ_inter
	sigma_rec
	eLJ11 eLJ12 ...
	eLJ21 eLJ22
	...     ...
	============================================================
	*/
	input >> do_Rec_LJ_inter;
	input >> sigma_rec;
	sigma_rec_2 = sigma_rec * sigma_rec;
	rcut_Rec_Rec = sigma_rec * 2.5;
	rcut_Rec_Rec_2 = rcut_Rec_Rec * rcut_Rec_Rec;
	std::getline(input, tmp_str);
	if (tmp_str == "\r" || tmp_str == "")
	{
		std::getline(input, tmp_str); // second one needed becaues of "\r", maybe problem on linux written input????
	}

	for (int i = 1; i < nr_Rec_types + 1; i++)
	{
		for (int j = 1; j < nr_Rec_types + 1; j++)
		{
			input >> Rec_Rec_inter_matrix[i][j];
			pot_shift_Rec_Rec_matrix[i][j] = Lennard_Jones_r2(rcut_Rec_Rec_2, sigma_rec_2, Rec_Rec_inter_matrix[i][j]);
		}
	}




	
	input.close();


	// initialisation
	Make_Cell_List();
	init_Rec_lists(); 
	Init_consts(); //init cell lists, consts
	
	std::ofstream log_out;
	log_out.open("log.txt");
	log_out.close();
	if (init_from_file)
	{
		Read_initail_conf_poly();
	}
	else
	{
		init_Receptors();
	}


}

bool is_empty(std::ifstream& pFile)
{
	return pFile.peek() == std::ifstream::traits_type::eof();
}

void Simulation::Read_initail_conf_poly()
{
	std::string pdb_fname = "system.inp";
	std::ifstream sys;
	sys.open(pdb_fname);
	std::string tmp_str;
	//std::string TER_str = "TER";
	std::string CON_str = "CONNECT";



	std::array<std::string, 6> L_arr = { "L0","L1","L2" ,"L3","L4", "L5" };
	std::array<std::string, 6> R_arr = { "R0","R1","R2" ,"R3","R4", "R5" };
	//init
	total_N_Rec = 0;
	N_rec_per_type = {0,0,0,0,0,0};
	


	int blob_c = 0;

	int lig_id = 0;
	int rec_id = 0;
	if (!is_empty(sys))
	{
		while (!sys.eof())
		{
			std::getline(sys, tmp_str);

			/*
			std::cout << i << " " << tmp_str << "\n";
			i++;

			if (tmp_str.find(TER_str) != std::string::npos)
			{
				std::cout << "Ter: "  << '\n';

				int a = 1;
			}*/
			if (blob_c == Blobs_in_poly)
			{
				blob_c = 0;
			}

			for (int lig_typ = 0; lig_typ < 6; lig_typ++)
			{
				if (tmp_str.find(L_arr[lig_typ]) != std::string::npos)
				{
					blob_c++;
					if (blob_c == 1)
					{
						nr_Poly++;
						lig_id = 0;
					}
					std::string x_str = tmp_str.substr(31, 7);
					std::string y_str = tmp_str.substr(39, 7);
					std::string z_str = tmp_str.substr(47, 7);
					Polymers[nr_Poly - 1][blob_c - 1] = { std::stod(x_str),std::stod(y_str),std::stod(z_str) };
					// Ligand bookeeping
					if (lig_typ != 0)
					{
						Ligands_in_Blob[nr_Poly - 1][blob_c - 1][0]++;
						Ligands_in_Blob[nr_Poly - 1][blob_c - 1][Ligands_in_Blob[nr_Poly - 1][blob_c - 1][0]] = lig_id;
						Ligands_in_Poly[nr_Poly - 1][lig_id] = { blob_c - 1 ,Ligands_in_Blob[nr_Poly - 1][blob_c - 1][0] };
						Ligands_Types_Bonds[nr_Poly - 1][lig_id] = { lig_typ ,-1 };


						Ligands_N_in_Poly[nr_Poly - 1]++;
						lig_id++;
					}


					// cell list bookeeping

					int cell_uid = (nr_Poly - 1) * Blobs_in_poly + blob_c - 1;
					int x_cell_n = (int)floor((Polymers[nr_Poly - 1][blob_c - 1][0]) / cell_side[0]);
					int y_cell_n = (int)floor((Polymers[nr_Poly - 1][blob_c - 1][1]) / cell_side[1]);
					int z_cell_n = (int)floor((Polymers[nr_Poly - 1][blob_c - 1][2]) / cell_side[2]);

					if (z_cell_n < 0)
					{
						z_cell_n = 0;
					}
					else if (z_cell_n > N_cells[2] - 1)
					{
						z_cell_n = N_cells[2] - 1;
					}

					std::array<int, 3> xyz_cell_n = { x_cell_n ,y_cell_n ,z_cell_n };


					cell_list_id_cell_xyz[cell_uid] = xyz_cell_n;


					int N_new_cell = cell_list_cell_xyz_N_cell[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]];

					cell_list_cell_xyz_ids[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]][N_new_cell] = cell_uid;


					cell_list_cell_xyz_N_cell[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]]++;
					/*
					*
					* //  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
					* 	  std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_in_Poly{};
						  std::array<int,  max_poly_in_sim> Ligands_N_in_Poly{};

						  // Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
						  std::array<std::array<std::array<int, max_ligands_in_poly + 1>, max_blobs_in_poly>, max_poly_in_sim> Ligands_in_Blob{};


						  std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_Types_Bonds{}; // Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}


	std::array<std::array<std::array<std::array<int, max_blobs_in_cell>, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_ids;
	std::array< std::array<int, 3>, max_total_blobs> cell_list_id_cell_xyz;
	std::array<std::array<std::array<int, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_N_cell;
					*
					*
					*/


				}
			}

			// Receptors
			for (int rec_typ = 0; rec_typ < 6; rec_typ++)
			{
				if (tmp_str.find(R_arr[rec_typ]) != std::string::npos)
				{
					if (rec_typ != 0)
					{
						//std::cout << tmp_str << "\n";
						std::string x_str = tmp_str.substr(31, 7);
						std::string y_str = tmp_str.substr(39, 7);
						Receptors[rec_id] = { std::stod(x_str),std::stod(y_str) };

						Receptor_Types_Bonds[rec_id] = { rec_typ,-1,-1 };
						total_N_Rec++;
						N_rec_per_type[rec_typ]++;
						N_rec_per_type[0]++;
						//double total_N_Rec = 50; //init from input
						//std::array<int, max_Rec_types + 1> N_rec_per_type = {}; //init from input
						// std::array<std::array<int,3>, max_N_rec> Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}


						// Rec Cell List
						int x_rcell = (int)floor((Receptors[rec_id][0]) / rec_cell_side[0]);
						int y_rcell = (int)floor((Receptors[rec_id][1]) / rec_cell_side[1]);
						Rec_Cell_List_cell_xy_rid[x_rcell][y_rcell][Rec_Cell_List_cell_xy_N[x_rcell][y_rcell]] = rec_id;
						Rec_Cell_List_cell_xy_N[x_rcell][y_rcell]++;
						Rec_Cell_List_rid_cell_xy[rec_id] = { x_rcell ,y_rcell };
						rec_id++;
						break;

					}

				}
			}


		}
	}
	Check_Ligand_lists();
	Check_Cell_List();
	Check_Rec_Cell_List();
	sys.close();


	std::string bonds_fname = "bonds.inp";
	std::ifstream bonds;
	bonds.open(bonds_fname);
	if (!is_empty(bonds))
	{
		while (!bonds.eof())
		{

			int rec_id;
			int rec_bound_poly;
			int rec_bound_blob;
			bonds >> rec_id;
			bonds >> rec_bound_poly;
			bonds >> rec_bound_blob;

			int lig_id;
			int Nlig_in_blob = Ligands_in_Blob[rec_bound_poly][rec_bound_blob][0];
			for (int i = 1; i < Nlig_in_blob + 1; i++)
			{
				lig_id = Ligands_in_Blob[rec_bound_poly][rec_bound_blob][i];
				int b_receptor_id = Ligands_Types_Bonds[rec_bound_poly][lig_id][1];
				if (b_receptor_id == -1)
				{
					Ligands_Types_Bonds[rec_bound_poly][lig_id][1] = rec_id;

					Receptor_Types_Bonds[rec_id][1] = rec_bound_poly;
					Receptor_Types_Bonds[rec_id][2] = lig_id;

					nr_Bonds_Rec[Receptor_Types_Bonds[rec_id][0]]++;
					nr_Bonds_Lig[Ligands_Types_Bonds[rec_bound_poly][lig_id][0]]++;

					nr_Bonds++;
					if (N_bonds_in_chain[rec_bound_poly] == 0)
					{
						nr_Bound_Poly++;
					}
					N_bonds_in_chain[rec_bound_poly]++;
					break;
				}
			}
		}
	}
	bonds.close();
	
	Check_Bonds();
	std::ofstream log_out;
	log_out.open("log.txt", std::ios_base::app);
	log_out << "================= READ DATA FROM FILE =================\n";
	log_out << "Found " << nr_Poly << " Polymers and " << total_N_Rec << " Receptors with " << nr_Bonds << " Bonds\n";
	log_out.close();

/*
*  Receptor, Ligand, Bond Bookeeping:
* 
* std::array<REC, max_N_rec> Receptors;
* std::array<std::array<int, 3>, max_N_rec> Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}
* std::array<int, max_poly_in_sim> Ligands_N_in_Poly;
* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_in_Poly;     //  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
* std::array<std::array<std::array<int, max_ligands_in_poly + 1>, max_blobs_in_poly>, max_poly_in_sim> Ligands_in_Blob;       // Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_Types_Bonds;       // Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}
* 
* 
*/
	
	


}





void Simulation::Output(uint64_t cycle, double avg_nr_poly_out_cycle, double avg_U_total_per_poly_out_cycle, double exe_time,double avg_nr_Bonds_per_poly_per_out_cycle,double nr_Bound_Poly_out_cycles_sum)
{
	std::ofstream log_out;
	log_out.open("log.txt", std::ios_base::app);
	log_out << "---------------CYCLE----" << cycle << "/" << total_nr_cycles << "-----------------" << "\n";
	log_out << "Execution time: " << exe_time << " s" << "\n";
	//log_out << "total_U old new : " << total_U << " " << Total_Energy() << " | bond old new:  " << total_bond_U << " " << Calc_bond_energy() << "\n";

	log_out << "Avg. Number of Polymers: " << avg_nr_poly_out_cycle << "\n";
	log_out << "Avg. Number of Bonds per Polymers: " << avg_nr_Bonds_per_poly_per_out_cycle << "\n";
	log_out << "Avg. Number of Bound Polymers: " << nr_Bound_Poly_out_cycles_sum << "\n";

	log_out << "Avg. Total Energy per Polymer: " << avg_U_total_per_poly_out_cycle << "\n";
	
	log_out << "frac. accepted steps: " << "  move Blob: " << nr_acc_MC_move_Blob_cycle / nr_tot_MC_move_Blob_cycle << "  insert Poly: " << nr_acc_MC_RB_ins_cycle / nr_tot_MC_RB_ins_cycle << "  delete Poly: " << nr_acc_MC_RB_del_cycle / nr_tot_MC_RB_del_cycle << "  move Rec: " << nr_acc_MC_move_Rec_cycle / nr_tot_MC_move_Rec_cycle << "\n";

	log_out << "--------------------------------------------------------" << "\n\n";

	log_out.close();
	
	
	if (output_last_conf)
	{
		// output the configuration at every step
		std::string pdb_fname = "system.pdb";
		Output_system(pdb_fname);
		std::string bonds_fname = "bonds.txt";
		Output_bonds(bonds_fname);

	}

	/*

	if (output_last_conf)
	{
		std::string pdb_fname = "system" +std::to_string(cycle)+ ".pdb";
		Output_system(pdb_fname);

	}*/

	/*
	std::ofstream energ;
	energ.open("energy.txt", std::ios_base::app);
	energ << cycle << " " << avg_U_total_per_poly_out_cycle << "\n";
	energ.close();
	*/

	std::ofstream npy;
	npy.open("nr_Poly.txt", std::ios_base::app);
	npy << cycle << " " << avg_nr_poly_out_cycle << " " << nr_Bound_Poly_out_cycles_sum << " " << avg_nr_Bonds_per_poly_per_out_cycle << "\n";
	npy.close();

}




void Simulation::Start_Output()
{
	std::ofstream log_out;
	log_out.open("log.txt", std::ios_base::app);

	log_out << "==========================INPUT==========================" << "\n";
	log_out << "-------------Simulation Lenght parameters-------------------\n";
	log_out << "Total number of MC cycles: " << total_nr_cycles << "\n";
	log_out << "Fraction of cycles per output: " << out_frac_cycles << "\n";
	log_out << "Ekvalibration fraction of total cycles: " << ekv_frac << "\n";

	log_out << "------------------- Simulation parameters -------------------\n";
	log_out << "Simulation Box x,y,z: " << box_x << " " << box_y << " " << box_z << "\n";
	log_out << "Chemical potential of ideal chains, mu: " << mu << "\n";
	log_out << "Bending stiffnes parameter, eps_b: " << eps_b << "\n";
	log_out << "Number of trial moves in RB scheme, k: " << k << "\n";
	log_out << "Maximum displacement of blob in MC moves, max_move_blob: " << max_move_blob << "\n";
	log_out << "Fraction of MC moves that insert/delete polymers: " << frac_ins_del_moves << "\n";
	log_out << "Fraction of MC moves move receptors: " << frac_rec_moves << "\n";
	log_out << "Random Seed: " << random_seed << "\n";

	log_out << "----------Polymer, Ligand and Receptor Parameters----------\n";

	log_out << "Number of Ligand, Receptror types: "<< nr_Lig_types <<" " << nr_Rec_types << "\n";
	log_out << "Number of Blobs per Polymer: " << Blobs_in_poly << "\n";
	log_out << "Number of Ligands per type per Polymer:";
	for (int i = 1; i < nr_Lig_types+1; i++)
	{
		log_out << " " << N_lig_per_type_per_poly[i];
	}
	log_out << "\n";
	log_out << "Block copolymers: " << block_copoly << "\n";
	log_out << "Possion distributed numbers of ligands: " << poiss_ligand_dist << "\n";
	log_out << "Number of Receptors per type:";
	for (int i = 1; i < nr_Rec_types + 1; i++)
	{
		log_out << " " << N_rec_per_type[i];
	}
	log_out << "\n";
	log_out << "---------------- Lig-Rec interaction matrix ------------------\n";

	for (int i = 1; i < nr_Lig_types + 1; i++)
	{
		for (int j = 1; j < nr_Rec_types + 1; j++)
		{
			log_out << " " << Lig_Rec_inter_matrix[i][j];
		}
		log_out << "\n";
	}

	log_out << "Ligand Receptor bond cutoff: " << rcut_Lig_Rec << "\n";
	log_out << "Receptor hard sphere radius, only between receptors: " << Rec_HS_rad << "\n";

	log_out << "------------------------------------------------------" << "\n";
	log_out << "Size of Cell x,y,z: " << cell_side[0] << " " << cell_side[1] << " " << cell_side[2] << "\n";
	log_out << "Size of Receptor Cell x,y: " << rec_cell_side[0] << " " << rec_cell_side[1]  << "\n";

	if (do_Rec_LJ_inter)
	{
		log_out << "Receptor sigma: " << sigma_rec << "\n";
		log_out << "---------------- Rec-Rec epsilon matrix ------------------\n";
		for (int i = 1; i < nr_Lig_types + 1; i++)
		{
			for (int j = 1; j < nr_Rec_types + 1; j++)
			{
				log_out << " " << Rec_Rec_inter_matrix[i][j];
			}
			log_out << "\n";
		}

	}

	log_out << "==========================START==========================" << "\n";

	log_out.close();


	/*
	std::ofstream energ;
	energ.open("energy.txt");
	energ.close();
	*/

	std::ofstream npy;
	npy.open("nr_Poly.txt");
	npy.close();

}
void Simulation::End_Output(double avg_nr_poly, double avg_U_total_per_poly, double total_exe_time, double avg_nr_Bonds_per_poly, std::array<double, max_Rec_types + 1> nr_Bonds_Lig_cycle_sum, std::array<double, max_Rec_types + 1> nr_Bonds_Rec_cycle_sum,double avg_nr_Bound_Poly)
{



	double Rg = pow(Blobs_in_poly, 0.588);
	double poly_vol = 4. * pi / 3. * pow(Rg, 3);
	double a = pow(poly_vol, 1. / 3.);
	double volume = box_x * box_y * box_z;
	double xy_surf = box_x * box_y;

	double poly_vol_frac = avg_nr_poly / volume * poly_vol;
	double bound_surf_frac = avg_nr_Bound_Poly / xy_surf * a * a;


	std::ofstream log_out;
	log_out.open("log.txt", std::ios_base::app);
	log_out << "==========================END==========================" << "\n";
	log_out << "Total Execution time: " << total_exe_time << " s" << "\n";
	log_out << "total_U old new : " << total_U << " "<< Total_Energy() << " | bond old new:  " << total_bond_U << " " << Calc_bond_energy() << " | rec old new:  " << total_rec_U << " " << Total_Rec_Energy() << "\n";
	log_out << "Avg. number of polymers: " << avg_nr_poly << "\n";	
	log_out << "Avg. conc. of polymers: " << avg_nr_poly / (box_x * box_y * box_z) << " Poly/rg^3" << "\n";
	log_out << "Avg. conc. of polymers: " << poly_vol_frac << " per polymer volume" << "\n";
	log_out << "Avg. energy per polymer: " << avg_U_total_per_poly << "\n";
	log_out << "frac. Accepted Moves:    " << "  Move Blob: " << nr_acc_MC_move_Blob / nr_tot_MC_move_Blob << "  Insert Poly: " << nr_acc_MC_RB_ins / nr_tot_MC_RB_ins << "  Delete Poly: " << nr_acc_MC_RB_del / nr_tot_MC_RB_del << "  Move Rec: " << nr_acc_MC_move_Rec / nr_tot_MC_move_Rec << "\n";
	log_out << "Avg. nr. Bonds per polymers: " << avg_nr_Bonds_per_poly << "\n";
	log_out << "Surface coverage: " << bound_surf_frac << "\n";
	log_out << "Avg. nr. Bound Polymers: " << avg_nr_Bound_Poly << "\n";
	log_out << "Avg. nr. Bonds per polymers per ligand type: \n";
	for (int i = 1; i < nr_Lig_types + 1; i++)
	{
		log_out << " " << nr_Bonds_Lig_cycle_sum[i];
	}
	log_out << "\nAvg. nr. Bonds per polymers per receptor type: \n";

	for (int i = 1; i < nr_Rec_types + 1; i++)
	{
		log_out << " " << nr_Bonds_Rec_cycle_sum[i] ;
	}
	log_out << "\n";
	
	log_out << "Avg. dist between neibouring blobs: " << dist_blob_blob_bond_sum << "\n"; // average distance between blobs
	log_out << "Avg. cos(theta): " << cos_theta_sum << "\n"; // average cos(theta)
	log_out << "Avg. theta: " << theta_sum * 180 / pi << "\n"; // average theta
	log_out << "Persistence lenght (cos(theta)): " <<  -dist_blob_blob_bond_sum / log(cos_theta_sum) << "\n"; // persistence lenght
	log_out << "Persistence lenght (sum li.l1):  "  << sum_sum_l_dot_l / dist_blob_blob_bond_sum << "\n"; // persistence lenght - sum calc
	log_out << "Avg. end to end dist: " << end_to_end_dist_sum << "\n"; // average end to end dist
	log_out << "Avg. square end to end dist:  " << end_to_end_dist_2_sum << "\n"; // square end to end dist
	log_out << "Avg. Rg: " << Rg_sum << "\n"; // Radius of gyration
	log_out << "Ideal Rg: " << Rg << "\n"; // Radius of gyration



	log_out << "====================================================" << "\n\n";
	log_out.close();





	std::ofstream avgs;
	avgs.open("averages.txt");
	avgs << std::setprecision(9) << poly_vol_frac << "\n";
	avgs << std::setprecision(9) << bound_surf_frac << "\n";
	avgs << std::setprecision(9) << avg_nr_poly << "\n";
	avgs << std::setprecision(9) << avg_nr_Bound_Poly << "\n";
	avgs << std::setprecision(9) << avg_nr_Bonds_per_poly << "\n";
	avgs << std::setprecision(9) << dist_blob_blob_bond_sum << "\n"; // average distance between blobs
	avgs << std::setprecision(9) << cos_theta_sum << "\n"; // average cos(theta)
	avgs << std::setprecision(9) << theta_sum * 180 / pi << "\n"; // average theta
	avgs << std::setprecision(9) << -dist_blob_blob_bond_sum / log(cos_theta_sum) << "\n"; // persistence lenght
	avgs << std::setprecision(9) << sum_sum_l_dot_l / dist_blob_blob_bond_sum << "\n"; // persistence lenght - sum calc
	avgs << std::setprecision(9) << end_to_end_dist_sum << "\n";   // average end to end dist
	avgs << std::setprecision(9) << end_to_end_dist_2_sum << "\n"; // square end to end dist
	avgs << std::setprecision(9) << Rg_sum << "\n"; // Radius of gyration

	avgs.close();

	if (output_last_conf)
	{
		std::string pdb_fname = "system.pdb";
		Output_system(pdb_fname);
		std::string bonds_fname = "bonds.txt";
		Output_bonds(bonds_fname);

	}
	if (more_outputs)
	{
		std::ofstream dist;
		dist.open("dist_blob_blob_bins.txt");
		for (int i = 0; i < total_nr_dist_bins; i++)
		{
			dist << (i+0.5)*dist_bin_width <<" " <<dist_blob_blob_bond_bins[i] << "\n";
		}
		dist.close();

		std::ofstream ctheta;
		ctheta.open("theta_bins.txt");
		for (int i = 0; i < total_nr_cos_theta_bins; i++)
		{
			ctheta << (i + 0.5) * theta_bin_width << " " << theta_bins[i] <<" " << start_cos_theta_bin + (i + 0.5) * cos_theta_bin_width << " " << cos_theta_bins[i] << "\n";
		}
		ctheta.close();
		
		std::ofstream lig_dist;
		lig_dist.open("lig_dist_ins.txt");

		// distribution of the numbers of ligands on polymers inserted
		for (int i_blob = 0; i_blob < Blobs_in_poly +1; i_blob++)
		{
			lig_dist << i_blob << " ";
			for (int type = 1; type < nr_Lig_types + 1; type++)
			{
				lig_dist << lig_in_poly_counts_insert[type][i_blob] / nr_acc_MC_RB_ins << " ";
			}
			lig_dist << "\n";
		}
		lig_dist.close();

		std::ofstream lig_dist_sim;
		lig_dist_sim.open("lig_dist_sim.txt");
		// distribution of the numbers of ligands on polymers in simulation
		for (int i_blob = 0; i_blob < Blobs_in_poly + 1; i_blob++)
		{
			lig_dist_sim << i_blob << " ";
			for (int type = 1; type < nr_Lig_types + 1; type++)
			{
				lig_dist_sim << lig_in_poly_counts_in_sim[type][i_blob] / count_lig_in_poly_sum << " ";
			}
			lig_dist_sim << "\n";
		}
		lig_dist_sim.close();


		// g(r) rec rec output
		for (int type_i = 1; type_i < nr_Lig_types + 1; type_i++)
		{
			for (int type_j = 1; type_j < nr_Lig_types + 1; type_j++)
			{
				double rho_0 = N_rec_per_type[type_j] / box_x / box_y;

								
				if (type_j == type_i)
				{
					rho_0 = (N_rec_per_type[type_j] - 1) / box_x / box_y;
				}
				
				std::ofstream g_r_out;
				std::string fname = "g_r_rec_rec_" + std::to_string(type_i) + std::to_string(type_j) + ".txt";

				g_r_out.open(fname);

				for (int i = 0; i < total_nr_g_r_rec_rec_bins; i++)
				{
					double g_r = g_r_rec_rec[type_i][type_j][i] / count_g_r_rec_rec[type_i] / (pi * g_r_rec_rec_bin_width * g_r_rec_rec_bin_width * ((i + 1) * (i + 1) - i * i)) / rho_0;;
					double r = (i + 0.5) * g_r_rec_rec_bin_width;
					g_r_out << r << " " << g_r << "\n";

				}

				g_r_out.close();

			}
		}
	}



	

}


void Simulation::Output_bonds(std::string& filename)
{
	std::ofstream bonds;
	bonds.open(filename);

	for (int rec_i = 0; rec_i < total_N_Rec; rec_i++)
	{
		//int rec_type = Receptor_Types_Bonds[rec_i][0];
		int rec_bond_poly = Receptor_Types_Bonds[rec_i][1];
		int rec_bond_lig = Receptor_Types_Bonds[rec_i][2];

		if (rec_bond_poly != -1)
		{
			//  Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}
			//  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
			int rec_bound_blob = Ligands_in_Poly[rec_bond_poly][rec_bond_lig][0];

			bonds << rec_i << " " << rec_bond_poly << " " << rec_bound_blob << "\n";
		}

	}

	bonds.close();

}

void Simulation::Output_system(std::string& filename)
{
	//int N_tot = nr_Poly * Blobs_in_poly + total_N_Rec;
	std::ofstream system;
	system.open(filename);

	//system << N_tot << "\n";

	//system << "SB MV MC system\n";

	char buffer[100];
	std::array<std::string, 6> lig_type_elements = {"C","N","O","S","P","Se"};
	int n = 0;
	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{

		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{
			int lig_type = 0;
			int N_ligand_blob = Ligands_in_Blob[poly_i][blob_i][0];
			if (N_ligand_blob != 0)
			{
				int lig_id = Ligands_in_Blob[poly_i][blob_i][1];
				lig_type = Ligands_Types_Bonds[poly_i][lig_id][0];
			}


			BLOB blob = Polymers[poly_i][blob_i];
			std::string l_t = "L" + std::to_string(lig_type);
			sprintf(buffer, "ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f\n",
				n, lig_type_elements[lig_type].c_str(), l_t.c_str(), "P", poly_i+1, blob[0], blob[1], blob[2]);

			system << buffer;
			if (blob_i != 0)
			{
				sprintf(buffer, "CONECT%5d%5d\n",
					n - 1, n);
				system << buffer;
			}
			n++;
		}
		//system << "\n\n";
	}
	std::array<std::string, 6> rec_type_elements = { "H","Na","K","Zn","Mn","Cu" };

	system << "TER" << "\n";
	for (int rec_i = 0; rec_i < total_N_Rec; rec_i++)
	{

		REC rec = Receptors[rec_i];
		int rec_type = Receptor_Types_Bonds[rec_i][0];
		std::string r_t = "R" + std::to_string(rec_type);

		sprintf(buffer, "ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f\n",
			n, rec_type_elements[rec_type].c_str(), r_t.c_str(), "R", nr_Poly+1, rec[0], rec[1], 0.0);

		system << buffer;

		n++;
		//recs << "\n\n";
	}
	system.close();






	/*
	int N_tot = nr_Poly * Blobs_in_poly + total_N_Rec;
	std::ofstream system;
	system.open(filename);

	system << N_tot << "\n";

	system << "SB MV MC system\n";

	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{
		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{
			int lig_type = 0;
			int N_ligand_blob = Ligands_in_Blob[poly_i][blob_i][0];
			if (N_ligand_blob != 0)
			{
				int lig_id  = Ligands_in_Blob[poly_i][blob_i][1];
				lig_type = Ligands_Types_Bonds[poly_i][lig_id][0];
			}


			BLOB blob = Polymers[poly_i][blob_i];
			system << "L" << lig_type << " " << blob[0] << " " << blob[1] << " " << blob[2] << "\n";
		}
		//system << "\n\n";
	}
	


	for (int rec_i = 0; rec_i < total_N_Rec; rec_i++)
	{

		REC rec = Receptors[rec_i];
		int rec_type = Receptor_Types_Bonds[rec_i][0];

		system << "R" << rec_type << " " << rec[0] << " " << rec[1] << " " << 0 << "\n";

		//recs << "\n\n";
	}
	system.close();*/

}