#pragma once
#include "Consts.h"
#include <iostream>
#include <cmath>
#include "random.h"
#include <array>

#define POLYMER std::array<std::array<double, 3>, max_blobs_in_poly>
#define MATRIX33 std::array<std::array<double, 3>, 3>
#define BLOB std::array<double, 3>
#define REC std::array<double, 2>

double Random_dist_Bond_Blob_Blob();  // Random distance using rejection method
void random_point_unit_sphere_MAR(double out_xyz[3]);  // Random point on a unit sphere, Marsaglia method

double dist_Blob_min_img_x_y(BLOB blob1, BLOB blob2, double box_x, double box_x_2, double box_y, double box_y_2);
// Distance between 2 blobs, x,y min image
double dist_2_Blob_min_img_x_y(BLOB blob1, BLOB blob2, double box_x, double box_x_2, double box_y, double box_y_2);


// randomly move blob at most +-max_move_blob PBC x,y
BLOB Random_move(BLOB blob,double max_move_blob, double box_x,  double  box_y); 


// makes a new blob randomly on a sphere radius with rejection method, PBC x,y
BLOB Random_Blob_close_sphere(BLOB blob1, double box_x, double  box_y);

// makes a new blob randomly on a sphere radius with rejection method, PBC x,y, theta with rejection method
BLOB Random_Blob_close_sphere_theta_bias(BLOB current_blob, BLOB prev_blob, double box_x, double  box_y, double max_p_theta, double eps_b);


double calc_max_P_theta(double eps_b); // calculates the maximum value of P(theta) for a particular eps_b, needed for efficeitn rejection sampling


MATRIX33 multi_33_matrix(MATRIX33 matrix1, MATRIX33 matrix2); //3x3 matrix multiplication
BLOB multi_31_matrix_vector(MATRIX33 matrix1, BLOB vector); // multiplication of 3x3 matrix and vector
BLOB cross_product(BLOB v_A, BLOB v_B); // corss product of vectors

MATRIX33 get_rot_matrix_a_to_b(BLOB a, BLOB b); // get rotation matrix that rotates vector a to b

BLOB sphere_to_cart(double r, double phi, double theta); // sphereical to cartesian coordinates


//blob interactions			// interactions in lenght r/rg of blob, units of k_bT
double interaction_Blob_Blob(double r); 
double interaction_Blob_Blob_bond(double r);
double interaction_Blob_Wall(double r);
double interaction_r2_Blob_Blob(double r2);


double interaction_r2_Bond_Tether(double r2);


double interaction_cos_theta_bending(double eps_b,double cos_theta);


double dot_prod(BLOB vec1, BLOB vec2); // BLOB used as a vector 3


double lenght(BLOB vec);


double Lennard_Jones_r2(double r2, double sigma2, double epsilon);
