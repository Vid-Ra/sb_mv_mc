#include "random.h"


/*
double ran_max = RAND_MAX;



double RandomDouble01() {

	int rn1 = rand();
	double rn = (static_cast <double> (rn1) / static_cast <double> (ran_max));
	return rn;

}

double RandomDouble11() {
	return RandomDouble01() * 2.0 - 1.0;
}

int RandomInt(int n)
{
	int rn = rand() % n;
	return rn;
}
*/

/*

const double ran_max = pow(2,32);
extern std::mt19937 generator;
std::uniform_real_distribution<double> distribution_0_1(0.0, 1.0);
std::uniform_real_distribution<double> distribution_1_1(-1.0, 1.0);



double RandomDouble01() {

	return distribution_0_1(generator);

}

double RandomDouble11() {
	return distribution_1_1(generator);
}

int RandomInt(int n)
{	
	uint_fast32_t rn1 = generator();
	return rn1 % n;
}
*/

const double ran_max = pow(2, 32);
extern std::mt19937 generator;
//std::uniform_real_distribution<double> distribution(0.0, 1.0);




double RandomDouble01() {

	double rn1 = double(generator()) / ran_max;
	//double rn1 = distribution(generator);
	/*if (rn1 < 0 || rn1 > 1)
	{
		std::cout << "rn1: " << rn1 << "\n";
	}*/	
	return rn1;

}

double RandomDouble11() {
	return RandomDouble01() * 2.0 - 1.0;
}

int RandomInt(int n)
{
	uint_fast32_t rn1 = generator();
	return rn1 % n;
}