#include "Simulation.h"


std::array<REC, max_N_rec> Receptors;
std::array<std::array<int,3>, max_N_rec> Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}

//  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_in_Poly{};
std::array<int,  max_poly_in_sim> Ligands_N_in_Poly{};

// Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
std::array<std::array<std::array<int, max_ligands_in_poly + 1>, max_blobs_in_poly>, max_poly_in_sim> Ligands_in_Blob{};


std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_Types_Bonds{}; // Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}

std::array<std::array<int, 2>, max_N_rec> Rec_Cell_List_rid_cell_xy{};
std::array<std::array<std::array<int, max_rec_in_cell>, max_cells>, max_cells> Rec_Cell_List_cell_xy_rid{};
std::array<std::array<int, max_cells>, max_cells> Rec_Cell_List_cell_xy_N{};

std::array<int, max_poly_in_sim> N_bonds_in_chain = {};


void init_Lig_lists()
{
	for (int poly_i = 0; poly_i < max_poly_in_sim; poly_i++)
	{
		for (int lig_i = 0; lig_i < max_ligands_in_poly; lig_i++)
		{
			Ligands_in_Poly[poly_i][lig_i] = { -1,-1 };
			Ligands_Types_Bonds[poly_i][lig_i] = { 0,-1 };
		}

		for (int blob_i = 0; blob_i < max_blobs_in_poly; blob_i++)
		{
			for (int lig_i = 0; lig_i < max_ligands_in_poly; lig_i++)
			{
				Ligands_in_Blob[poly_i][blob_i][lig_i] = -1;
			}
			Ligands_in_Blob[poly_i][blob_i][0] = 0;
		}
		
		Ligands_N_in_Poly[poly_i] = 0;


	}


}


double Simulation::dist_2_min_img_xy_rec_rec(REC rec1, REC rec2)
{
	double dx = rec1[0] - rec2[0];
	double dy = rec1[1] - rec2[1];
	if (dx < -box_x_2)
	{
		dx += box_x;
	}
	else if (dx > box_x_2)
	{
		dx -= box_x;

	}
	if (dy < -box_y_2)
	{
		dy += box_y;
	}
	else if (dy > box_y_2)
	{
		dy -= box_y;

	}
	return dx * dx + dy * dy ;
}


void Simulation::init_Rec_lists()
{
	init_Lig_lists();




	// init rec cell list
	for (int i = 0; i < max_N_rec; i++)
	{
		Rec_Cell_List_rid_cell_xy[i] = { -1,-1 };
	}

	for (int x = 0; x < max_cells; x++)
	{
		for (int y = 0; y < max_cells; y++)
		{
			for (int i = 0; i < max_rec_in_cell; i++)
			{
				Rec_Cell_List_cell_xy_rid[x][y][i] = -1;
			}
			Rec_Cell_List_cell_xy_N[x][y] = 0;
		}
	}

	//rec_N_cells[0] = (int)(box_x / 2.);
	rec_N_cells[0] = (int)(box_x / rcut_Lig_Rec);
	if (rec_N_cells[0] > max_cells)
	{
		rec_N_cells[0] = max_cells;
	}
	//rec_N_cells[1] = (int)(box_y / 2.);
	rec_N_cells[1] = (int)(box_y / rcut_Lig_Rec);
	if (rec_N_cells[1] > max_cells)
	{
		rec_N_cells[1] = max_cells;
	}


	rec_cell_side[0] = box_x / rec_N_cells[0];
	rec_cell_side[1] = box_y / rec_N_cells[1];


}



void Simulation::init_Receptors()
{
	


	int n_rec = 0;
	while (n_rec < total_N_Rec) //random receptor positions
	{
		bool overlap = false;
		double rn1 = RandomDouble01();
		double rn2 = RandomDouble01();
		REC tmp_rec = { rn1 * box_x,rn2 * box_y };

		for (int i = 0; i < n_rec; i++)
		{
			double dist_2 = dist_2_min_img_xy_rec_rec(Receptors[i], tmp_rec );

			if (dist_2 < 4 * Rec_HS_rad * Rec_HS_rad) // Hard spheres
			{
				overlap = true;
				break;
			}
		}
		if (!overlap)
		{	

			int x_rcell = (int)floor((tmp_rec[0]) / rec_cell_side[0]);
			int y_rcell = (int)floor((tmp_rec[1]) / rec_cell_side[1]);
			Rec_Cell_List_cell_xy_rid[x_rcell][y_rcell][Rec_Cell_List_cell_xy_N[x_rcell][y_rcell]] = n_rec;
			Rec_Cell_List_cell_xy_N[x_rcell][y_rcell]++;
			Rec_Cell_List_rid_cell_xy[n_rec] = { x_rcell ,y_rcell };
			Receptors[n_rec] = tmp_rec;
			n_rec++;
		}
	}
	n_rec = 0;
	for (int rec_type = 1; rec_type < nr_Rec_types + 1; rec_type++)
	{

		for (int i = 0; i < N_rec_per_type[rec_type]; i++)
		{
			Receptor_Types_Bonds[n_rec] = { rec_type,-1,-1 };
			n_rec++;
		}

	}
	//n_rec = 0;
	Check_Rec_Cell_List();

}

double Simulation::dist_2_min_img_xy_blob_rec(BLOB blob, REC rec)
{
	double dx = blob[0] - rec[0];
	double dy = blob[1] - rec[1];
	double dz = blob[2];
	if (dx < -box_x_2)
	{
		dx += box_x;
	}
	else if (dx > box_x_2)
	{
		dx -= box_x;

	}
	if (dy < -box_y_2)
	{
		dy += box_y;
	}
	else if (dy > box_y_2)
	{
		dy -= box_y;

	}
	return dx * dx + dy * dy + dz * dz;
}




int Simulation::Find_Rec_near_Blob(int out_rec_ids[],double out_rec_dists2[], BLOB blob)
{
	int n_close_rec = 0;


	int cell_x = (int)floor((blob[0]) / rec_cell_side[0]); // calculate rec cell of blob 
	int cell_y = (int)floor((blob[1]) / rec_cell_side[1]);

	for (int j_cell_tmp = cell_y - 1; j_cell_tmp < cell_y + 2; j_cell_tmp++)
	{
		int j_cell = j_cell_tmp;
		if (j_cell < 0)
		{
			j_cell += rec_N_cells[1];
		}
		else if (j_cell > rec_N_cells[1] - 1)
		{
			j_cell -= rec_N_cells[1];
		}
		for (int i_cell_tmp = cell_x - 1; i_cell_tmp < cell_x + 2; i_cell_tmp++) // loops over cells +-1
		{

			int i_cell = i_cell_tmp;

			if (i_cell < 0)
			{
				i_cell += rec_N_cells[0];
			}
			else if (i_cell > rec_N_cells[0] - 1)
			{
				i_cell -= rec_N_cells[0];
			}

			int N_rec_in_cell = Rec_Cell_List_cell_xy_N[i_cell][j_cell];
			for (int cell_rec_i = 0; cell_rec_i < N_rec_in_cell; cell_rec_i++) // loop over rec in specific cell
			{
				int rec_id = Rec_Cell_List_cell_xy_rid[i_cell][j_cell][cell_rec_i];
				
				if (Receptor_Types_Bonds[rec_id][1] == -1) // recepter unbound
				{
					double dist_2 = dist_2_min_img_xy_blob_rec(blob, Receptors[rec_id]);
					if (dist_2 < rcut_Lig_Rec_2) // receptor close enough
					{
						out_rec_ids[n_close_rec] = rec_id;
						out_rec_dists2[n_close_rec] = dist_2;
						n_close_rec++;
					}
				}

				
				
				

			}
		}
	}


	return n_close_rec;
}



REC Random_move_Rec(REC rec1, double max_move_rec, double box_x, double  box_y)
{
	double ranx = max_move_rec * RandomDouble11();
	double new_x = rec1[0] + ranx;

	double rany = max_move_rec * RandomDouble11();

	double new_y = rec1[1] + rany;
	if (new_x < 0)
	{
		new_x += box_x;
	}
	else if (new_x > box_x)
	{
		new_x -= box_x;

	}
	if (new_y < 0)
	{
		new_y += box_y;
	}
	else if (new_y > box_y)
	{
		new_y -= box_y;

	}
	/*
	new_x = new_x - box_x * floor(new_x / box_x);
	new_y = new_y - box_y * floor(new_y / box_y);
	*/


	REC out_rec{ new_x ,new_y };
	return out_rec;
}

bool Simulation::Check_Rec_Overlap(REC current_trial_rec, int current_rec_id)
{
	/*
	* Checks for receptor HS overlap using the receptor cell list
	*	- potential problem with very few large (larger than cell list side) receptors - seems unlikely to try such large rec.
	*/

	int cell_x = (int)floor((current_trial_rec[0]) / rec_cell_side[0]); // calculate rec cell of blob 
	int cell_y = (int)floor((current_trial_rec[1]) / rec_cell_side[1]);

	bool overlap = false;
	for (int j_cell_tmp = cell_y - 1; j_cell_tmp < cell_y + 2; j_cell_tmp++)
	{
		int j_cell = j_cell_tmp;
		if (j_cell < 0)
		{
			j_cell += rec_N_cells[1];
		}
		else if (j_cell > rec_N_cells[1] - 1)
		{
			j_cell -= rec_N_cells[1];
		}
		for (int i_cell_tmp = cell_x - 1; i_cell_tmp < cell_x + 2; i_cell_tmp++) // loops over cells +-1
		{

			int i_cell = i_cell_tmp;

			if (i_cell < 0)
			{
				i_cell += rec_N_cells[0];
			}
			else if (i_cell > rec_N_cells[0] - 1)
			{
				i_cell -= rec_N_cells[0];
			}

			int N_rec_in_cell = Rec_Cell_List_cell_xy_N[i_cell][j_cell];
			for (int cell_rec_i = 0; cell_rec_i < N_rec_in_cell; cell_rec_i++) // loop over rec in specific cell
			{
				int rec_id = Rec_Cell_List_cell_xy_rid[i_cell][j_cell][cell_rec_i];

				
				if (rec_id != current_rec_id) // do not check for overlap with current rec
				{

					double dist_2 = dist_2_min_img_xy_rec_rec(Receptors[rec_id], current_trial_rec);

					if (dist_2 < Rec_HS_overlap_2) // Hard spheres
					{
						overlap = true;
						return overlap;
					}

				}




			}
		}
	}
	return overlap;


}


void Simulation::MC_move_Rec_HS()
{
 /*
 * Monte Carlo move of receptor
 * If receptor is unbound, check HS overlap, accept move
 * If receptor bound, check HS overlap, check distance from blob, metropolis criterion on bond harmonic energy
 */
	
	if (total_N_Rec == 0) return;



	int ran_rec_id = RandomInt((int)total_N_Rec);
	REC current_rec = Receptors[ran_rec_id];
	REC trial_rec = Random_move_Rec(current_rec,  max_move_rec,  box_x,   box_y);

	bool accept_bool = false;
	double dU = 0;
	if (Receptor_Types_Bonds[ran_rec_id][1] == -1) // recepter unbound
	{
		bool overlap = Check_Rec_Overlap(trial_rec, ran_rec_id);
		accept_bool = !overlap;
	}
	else // bound receptor
	{
		/*
		*	Receptor, Ligand, Bond Bookeeping:
		* 
		* std::array<REC, max_N_rec> Receptors;
		* std::array<std::array<int, 3>, max_N_rec> Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}
		* std::array<int, max_poly_in_sim> Ligands_N_in_Poly;
		* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_in_Poly;     //  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
		* std::array<std::array<std::array<int, max_ligands_in_poly + 1>, max_blobs_in_poly>, max_poly_in_sim> Ligands_in_Blob;       // Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
		* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_Types_Bonds;       // Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}
		*/



		int bound_poly_id = Receptor_Types_Bonds[ran_rec_id][1];
		int bound_blob_id = Ligands_in_Poly[bound_poly_id][Receptor_Types_Bonds[ran_rec_id][2]][0];

		//int bound_blob_uid = bound_poly_id * Blobs_in_poly + bound_blob_id;

		BLOB bound_blob = Polymers[bound_poly_id][bound_blob_id];

		double dist_2_new = dist_2_min_img_xy_blob_rec(bound_blob, trial_rec);
		
		double dist_2_old = dist_2_min_img_xy_blob_rec(bound_blob, current_rec);

		if (dist_2_new > rcut_Lig_Rec_2)
		{
			//int a = 2;
			return;
		}
		

		double U_bond_new = interaction_r2_Bond_Tether(dist_2_new);
		double U_bond_old = interaction_r2_Bond_Tether(dist_2_old);
		dU = U_bond_new - U_bond_old;

		/* // gives equal random usage to LJ func, slightly less optimised probably
		bool overlap = Check_Rec_Overlap(trial_rec, ran_rec_id);

		if (overlap)
		{
			accept_bool = false;
		}
		else
		{
			double arg = exp(-dU);

			double ran = RandomDouble01();
			accept_bool = ran < arg;		// metropolis criterion


		}*/

		double arg = exp(-dU);

		double ran = RandomDouble01();



		// metropolis criterion
		if (ran < arg)	//accept
		{

			bool overlap = Check_Rec_Overlap(trial_rec, ran_rec_id);
			accept_bool = !overlap;

		}

	}






	nr_tot_MC_move_Rec++;
	nr_tot_MC_move_Rec_cycle++;


	if (accept_bool) // accept the move to trial rec, update rec cell list,
	{
		total_bond_U += dU;

		nr_acc_MC_move_Rec++;
		nr_acc_MC_move_Rec_cycle++;


		Receptors[ran_rec_id] = trial_rec;
		
		/*
		* Receptor Cell Lists
		* std::array<std::array<int, 2>, max_N_rec> Rec_Cell_List_rid_cell_xy;  
		* std::array<std::array<std::array<int, max_blobs_in_cell>, max_cells>, max_cells> Rec_Cell_List_cell_xy_rid;
		* std::array<std::array<int, max_cells>, max_cells> Rec_Cell_List_cell_xy_N;
		*/

		int cell_x = (int)floor((trial_rec[0]) / rec_cell_side[0]); // calculate rec cell of new rec
		int cell_y = (int)floor((trial_rec[1]) / rec_cell_side[1]);
		

		std::array<int, 2> rec_cell = { cell_x ,cell_y };

		std::array<int, 2> rec_cell_old = Rec_Cell_List_rid_cell_xy[ran_rec_id]; // rec cell of new rec
		int cell_x_old = rec_cell_old[0];
		int cell_y_old = rec_cell_old[1];
		
		
		if (rec_cell != rec_cell_old)
		{
			Rec_Cell_List_rid_cell_xy[ran_rec_id] = rec_cell;


			// and ran_rec_id to the end of the cell list of new cell
			int N_rec_new_cell = Rec_Cell_List_cell_xy_N[cell_x][cell_y];
			Rec_Cell_List_cell_xy_rid[cell_x][cell_y][N_rec_new_cell] = ran_rec_id;

			// swap last rec in old cell to the location of ran_rec_id
			int N_rec_old_cell = Rec_Cell_List_cell_xy_N[cell_x_old][cell_y_old];
			for (int i = 0; i < N_rec_old_cell; i++)
			{
				if (Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][i] == ran_rec_id)
				{
					Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][i] = Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][N_rec_old_cell - 1];
					Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][N_rec_old_cell - 1] = -1;
					break;

				}
			}

			// update number of rec in new and old cell
			Rec_Cell_List_cell_xy_N[cell_x][cell_y]++;
			Rec_Cell_List_cell_xy_N[cell_x_old][cell_y_old]--;

		}


	}



}

bool overlap_bool = false;


double Simulation::Calculate_receptor_LJ_interactions(REC current_trial_rec, int current_rec_id, bool count_g_rr = false)
{
	/*
	* Calculate LJ receptor interations using the receptor cell list
	*/
	overlap_bool = false;

	int cell_x = (int)floor((current_trial_rec[0]) / rec_cell_side[0]); // calculate rec cell of blob 
	int cell_y = (int)floor((current_trial_rec[1]) / rec_cell_side[1]);
	
	//Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}
	int rec_type_curr = Receptor_Types_Bonds[current_rec_id][0];

	double U_lj = 0;
	for (int j_cell_tmp = cell_y - 1; j_cell_tmp < cell_y + 2; j_cell_tmp++)
	{
		int j_cell = j_cell_tmp;
		if (j_cell < 0)
		{
			j_cell += rec_N_cells[1];
		}
		else if (j_cell > rec_N_cells[1] - 1)
		{
			j_cell -= rec_N_cells[1];
		}
		for (int i_cell_tmp = cell_x - 1; i_cell_tmp < cell_x + 2; i_cell_tmp++) // loops over cells +-1
		{

			int i_cell = i_cell_tmp;

			if (i_cell < 0)
			{
				i_cell += rec_N_cells[0];
			}
			else if (i_cell > rec_N_cells[0] - 1)
			{
				i_cell -= rec_N_cells[0];
			}

			int N_rec_in_cell = Rec_Cell_List_cell_xy_N[i_cell][j_cell];
			for (int cell_rec_i = 0; cell_rec_i < N_rec_in_cell; cell_rec_i++) // loop over rec in specific cell
			{
				int rec_id = Rec_Cell_List_cell_xy_rid[i_cell][j_cell][cell_rec_i];
				int rec_type_i = Receptor_Types_Bonds[rec_id][0];

				if (rec_id != current_rec_id) // do not calculate energy with current rec
				{

					double dist_2 = dist_2_min_img_xy_rec_rec(Receptors[rec_id], current_trial_rec);
					double U_rr = 0;
					if (dist_2 <  Rec_HS_overlap_2) // Hard spheres
					{
						 //U_rr = abs(Lennard_Jones_r2(dist_2, sigma_rec_2, Rec_Rec_inter_matrix[rec_type_curr][rec_type_i]) - pot_shift_Rec_Rec_matrix[rec_type_curr][rec_type_i])*2 + 1e31;
						U_rr = Lennard_Jones_r2(dist_2, sigma_rec_2, Rec_Rec_inter_matrix[rec_type_curr][rec_type_i]) - pot_shift_Rec_Rec_matrix[rec_type_curr][rec_type_i];
						overlap_bool = true;
					}
					else if (dist_2 < rcut_Rec_Rec_2) // Rcut
					{
						U_rr = Lennard_Jones_r2(dist_2, sigma_rec_2, Rec_Rec_inter_matrix[rec_type_curr][rec_type_i]) - pot_shift_Rec_Rec_matrix[rec_type_curr][rec_type_i];

					}

					if (count_g_rr)
					{

						int n_bin = round(sqrt(dist_2) / g_r_rec_rec_bin_width);
						if (n_bin >= total_nr_g_r_rec_rec_bins)
						{
							continue;
						}
						if (rec_type_i != rec_type_curr)
						{
							g_r_rec_rec[rec_type_curr][rec_type_i][n_bin]++;
							//g_r_rec_rec[rec_type_i][rec_type_curr][n_bin]++;

						}
						else
						{

							g_r_rec_rec[rec_type_curr][rec_type_i][n_bin]++;

						}

					}
					U_lj += U_rr;
				}
			}
		}
	}
	if (count_g_rr)
	{
		count_g_r_rec_rec[rec_type_curr]++;
	}

	return U_lj;
}



double Simulation::Total_Rec_Energy()
{
	double U_rec_rec = 0;

	for (int i = 0; i < total_N_Rec; i++)
	{
		int rec_type_i = Receptor_Types_Bonds[i][0];
		REC rec_i = Receptors[i];
		for (int j = i+1; j < total_N_Rec; j++)
		{
			int rec_type_j = Receptor_Types_Bonds[j][0];
			REC rec_j = Receptors[j];

			double dist_2 = dist_2_min_img_xy_rec_rec(rec_i, rec_j);

			if (dist_2 < rcut_Rec_Rec_2) // Rcut
			{
				double epsilon = Rec_Rec_inter_matrix[rec_type_i][rec_type_j];

				U_rec_rec += Lennard_Jones_r2(dist_2, sigma_rec_2, epsilon) - pot_shift_Rec_Rec_matrix[rec_type_i][rec_type_j];

			}
		}
	}
	return U_rec_rec;

}



void Simulation::MC_move_Rec_LJ()
{
	/*
	* Monte Carlo move of receptor
	* If receptor is unbound, calculate LJ energy, metropolis accept move
	* If receptor bound, calculate LJ energy, check distance from blob, metropolis criterion on bond harmonic energy
	*/

	if (total_N_Rec == 0) return;



	int ran_rec_id = RandomInt((int)total_N_Rec);
	REC current_rec = Receptors[ran_rec_id];
	REC trial_rec = Random_move_Rec(current_rec, max_move_rec, box_x, box_y);
	//double total_rec_U_1 = Total_Rec_Energy();

	double dU = 0;
	double dU_bond = 0;


	if (Receptor_Types_Bonds[ran_rec_id][1] != -1)  // bound receptor
	{
		/*
		*	Receptor, Ligand, Bond Bookeeping:
		*
		* std::array<REC, max_N_rec> Receptors;
		* std::array<std::array<int, 3>, max_N_rec> Receptor_Types_Bonds; // Receptor_Types_Bonds[recpetor_id] = {receptor_type,b_poly,b_ligand_id}
		* std::array<int, max_poly_in_sim> Ligands_N_in_Poly;
		* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_in_Poly;     //  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
		* std::array<std::array<std::array<int, max_ligands_in_poly + 1>, max_blobs_in_poly>, max_poly_in_sim> Ligands_in_Blob;       // Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
		* std::array<std::array<std::array<int, 2>, max_ligands_in_poly>, max_poly_in_sim> Ligands_Types_Bonds;       // Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}
		*/



		int bound_poly_id = Receptor_Types_Bonds[ran_rec_id][1];
		int bound_blob_id = Ligands_in_Poly[bound_poly_id][Receptor_Types_Bonds[ran_rec_id][2]][0];

		//int bound_blob_uid = bound_poly_id * Blobs_in_poly + bound_blob_id;

		BLOB bound_blob = Polymers[bound_poly_id][bound_blob_id];

		double dist_2_new = dist_2_min_img_xy_blob_rec(bound_blob, trial_rec);

		double dist_2_old = dist_2_min_img_xy_blob_rec(bound_blob, current_rec);

		if (dist_2_new > rcut_Lig_Rec_2)
		{
			//int a = 2;
			return;
		}

		double U_bond_new = interaction_r2_Bond_Tether(dist_2_new);
		double U_bond_old = interaction_r2_Bond_Tether(dist_2_old);
		dU_bond = U_bond_new - U_bond_old;

	}


	double U_rec_rec_new = Calculate_receptor_LJ_interactions(trial_rec, ran_rec_id);
	bool overlap_n = overlap_bool;
	double U_rec_rec_old = Calculate_receptor_LJ_interactions(current_rec, ran_rec_id,calc_g_r_rec_rec);
	bool overlap_o = overlap_bool;

	double dU_rec = U_rec_rec_new - U_rec_rec_old;
	dU = dU_rec + dU_bond;

	double accept_bool = true;
	if (overlap_n && overlap_o)
	{
		if (dU != 0)
		{
			double arg = exp(-dU);

			double ran = RandomDouble01();
			accept_bool = ran < arg;
		}


	}
	else if (overlap_n)
	{
		accept_bool = false;

	}
	else if (overlap_o)
	{
		accept_bool = true;

	}
	else
	{
		if (dU != 0)
		{
			double arg = exp(-dU);

			double ran = RandomDouble01();
			accept_bool = ran < arg;
		}


	}
	


	nr_tot_MC_move_Rec++;
	nr_tot_MC_move_Rec_cycle++;


	if (accept_bool) // accept the move to trial rec, update rec cell list,
	{
		total_bond_U += dU_bond;
		total_rec_U += dU_rec;
		nr_acc_MC_move_Rec++;
		nr_acc_MC_move_Rec_cycle++;


		Receptors[ran_rec_id] = trial_rec;

		/*
		* Receptor Cell Lists
		* std::array<std::array<int, 2>, max_N_rec> Rec_Cell_List_rid_cell_xy;
		* std::array<std::array<std::array<int, max_blobs_in_cell>, max_cells>, max_cells> Rec_Cell_List_cell_xy_rid;
		* std::array<std::array<int, max_cells>, max_cells> Rec_Cell_List_cell_xy_N;
		*/

		int cell_x = (int)floor((trial_rec[0]) / rec_cell_side[0]); // calculate rec cell of new rec
		int cell_y = (int)floor((trial_rec[1]) / rec_cell_side[1]);


		std::array<int, 2> rec_cell = { cell_x ,cell_y };

		std::array<int, 2> rec_cell_old = Rec_Cell_List_rid_cell_xy[ran_rec_id]; // rec cell of new rec
		int cell_x_old = rec_cell_old[0];
		int cell_y_old = rec_cell_old[1];


		if (rec_cell != rec_cell_old)
		{
			Rec_Cell_List_rid_cell_xy[ran_rec_id] = rec_cell;


			// and ran_rec_id to the end of the cell list of new cell
			int N_rec_new_cell = Rec_Cell_List_cell_xy_N[cell_x][cell_y];
			Rec_Cell_List_cell_xy_rid[cell_x][cell_y][N_rec_new_cell] = ran_rec_id;

			// swap last rec in old cell to the location of ran_rec_id
			int N_rec_old_cell = Rec_Cell_List_cell_xy_N[cell_x_old][cell_y_old];
			for (int i = 0; i < N_rec_old_cell; i++)
			{
				if (Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][i] == ran_rec_id)
				{
					Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][i] = Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][N_rec_old_cell - 1];
					Rec_Cell_List_cell_xy_rid[cell_x_old][cell_y_old][N_rec_old_cell - 1] = -1;
					break;

				}
			}

			// update number of rec in new and old cell
			Rec_Cell_List_cell_xy_N[cell_x][cell_y]++;
			Rec_Cell_List_cell_xy_N[cell_x_old][cell_y_old]--;

		}
		//double total_rec_U_2 = Total_Rec_Energy();
		//int a = 1;

	}



}





















double Simulation::Calc_bond_energy()
{
	
	double total_bond_E = 0;
	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{

		for (int lig_id = 0; lig_id < Ligands_N_in_Poly[poly_i]; lig_id++)
		{
			int lig_bond_rec = Ligands_Types_Bonds[poly_i][lig_id][1];
			if (lig_bond_rec != -1)
			{
				int blob_id = Ligands_in_Poly[poly_i][lig_id][0];
				REC bound_rec = Receptors[lig_bond_rec];
				BLOB bound_blob = Polymers[poly_i][blob_id];
				int rec_type = Receptor_Types_Bonds[lig_bond_rec][0];

				int lig_type = Ligands_Types_Bonds[poly_i][lig_id][0];
				
				double dist2 = dist_2_min_img_xy_blob_rec(bound_blob, bound_rec);

				double U_tet = interaction_r2_Bond_Tether(dist2);

				double bond_E = Lig_Rec_inter_matrix[lig_type][rec_type] + U_tet;
				total_bond_E += bond_E;
			}

		}
		
	}
	return total_bond_E;
}









void Simulation::Check_Ligand_lists()
{
	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{
		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{
			int N_lig_in_blob = Ligands_in_Blob[poly_i][blob_i][0];

			for (int ligand_in_blob_i = 1; ligand_in_blob_i < N_lig_in_blob+1; ligand_in_blob_i++)
			{
				int ligand_id = Ligands_in_Blob[poly_i][blob_i][ligand_in_blob_i];

				
				int Blob_id = Ligands_in_Poly[poly_i][ligand_id][0];
				int ligand_in_blob_id = Ligands_in_Poly[poly_i][ligand_id][1];

				if (Blob_id != blob_i || ligand_in_blob_id != ligand_in_blob_i)
				{
					std::cout << "Error 1 in Ligand lists " << "\n";
				}
			}
		}


		int N_lig_in_poly = Ligands_N_in_Poly[poly_i];

		for (int ligand_id = 0; ligand_id < N_lig_in_poly; ligand_id++)
		{
			int blob_id = Ligands_in_Poly[poly_i][ligand_id][0];
			int ligand_in_blob_id = Ligands_in_Poly[poly_i][ligand_id][1];


			int ligand_i = Ligands_in_Blob[poly_i][blob_id][ligand_in_blob_id];

			if (ligand_i != ligand_id)
			{
				std::cout << "Error 2 in Ligand lists "<< ligand_i << " "<< ligand_id << "\n";

			}

		}
	}
}

void Simulation::Check_Bonds()
{
	double n_bonds = 0;
	for (int rec_i = 0; rec_i < total_N_Rec; rec_i++)
	{
		//int rec_type = Receptor_Types_Bonds[rec_i][0];
		int rec_bond_poly = Receptor_Types_Bonds[rec_i][1];
		int rec_bond_lig = Receptor_Types_Bonds[rec_i][2];

		if (rec_bond_poly != -1)
		{
			int rec_id_back = Ligands_Types_Bonds[rec_bond_poly][rec_bond_lig][1];
			n_bonds++;
			if (rec_id_back != rec_i)
			{
				std::cout << "Error 1 in Bond lists " << rec_id_back << " " << rec_i << "\n";

			}
		}

	}
	if (n_bonds != nr_Bonds)
	{
		std::cout << "Error 3 n bonds " << n_bonds << " " << nr_Bonds << "\n";

	}

	n_bonds = 0;
	int nr_bound_poly = 0;
	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{
		int n_bonds_chain = 0;
		int poly_bound = 0;
		for (int lig_id = 0; lig_id < Ligands_N_in_Poly[poly_i]; lig_id++)
		{
			int lig_bond_rec = Ligands_Types_Bonds[poly_i][lig_id][1];
			if (lig_bond_rec != -1)
			{
				poly_bound = 1;
				int lig_id_back = Receptor_Types_Bonds[lig_bond_rec][2];
				int poly_id_back = Receptor_Types_Bonds[lig_bond_rec][1];
				n_bonds++;
				n_bonds_chain++;
				if (poly_id_back != poly_i || lig_id != lig_id_back)
				{
					std::cout << "Error 2 in Bond lists "  << "\n";

				}
			}

		}
		if (poly_bound == 1)
		{
			nr_bound_poly++;
		}

		if (n_bonds_chain != N_bonds_in_chain[poly_i])
		{
			std::cout << "Error 5 n bonds chain" << n_bonds_chain << " " << N_bonds_in_chain[poly_i] << "\n";

		}
	}
	if (nr_bound_poly != nr_Bound_Poly)
	{
		std::cout << "Error 6 nr_bound_poly " << nr_bound_poly << " " << nr_Bound_Poly << "\n";

	}
	if (n_bonds != nr_Bonds)
	{
		std::cout << "Error 4 n bonds " << n_bonds << " " << nr_Bonds << "\n";

	}

}



void Simulation::Check_Rec_Cell_List()
{
	std::ofstream log_out;
	log_out.open("log.txt", std::ios_base::app);
	for (int rec_i = 0; rec_i < total_N_Rec; rec_i++)
	{
		REC rec = Receptors[rec_i];
		int cell_x = (int)floor((rec[0]) / rec_cell_side[0]); // calculate rec cell of blob 
		int cell_y = (int)floor((rec[1]) / rec_cell_side[1]);

		/*	int x_rcell = (int)floor((tmp_rec[0]) / rec_cell_side[0]);
			int y_rcell = (int)floor((tmp_rec[1]) / rec_cell_side[1]);
			Rec_Cell_List_cell_xy_rid[x_rcell][y_rcell][Rec_Cell_List_cell_xy_N[x_rcell][y_rcell]] = n_rec;
			Rec_Cell_List_cell_xy_N[x_rcell][y_rcell]++;
			Rec_Cell_List_rid_cell_xy[n_rec] = { x_rcell ,y_rcell };
			Receptors[n_rec] = tmp_rec;
		*/

		std::array<int, 2> cell_xy = { cell_x, cell_y };
		if (Rec_Cell_List_rid_cell_xy[rec_i] != cell_xy)
		{
			log_out << "Error with rec cell list " << cell_xy[0] << " " << cell_xy[1] << " "  << Rec_Cell_List_rid_cell_xy[rec_i][0] << " " << Rec_Cell_List_rid_cell_xy[rec_i][1] << "\n";
		}

		bool bl = true;

		for (int i = 0; i < Rec_Cell_List_cell_xy_N[cell_x][cell_y]; i++)
		{
			if (Rec_Cell_List_cell_xy_rid[cell_x][cell_y][i] == rec_i)
			{
				bl = false;
				break;
			}
		}
		if (bl)
		{
			log_out << "Error with cell list, no rec i in Rec_Cell_List_cell_xy_rid " << cell_xy[0] << " " << cell_xy[1] << "\n";

			
		}
	}
	int nrec = 0;
	for (int i = 0; i < max_cells; i++)
	{
		for (int j = 0; j < max_cells; j++)
		{
			int n_rec_in_cell = Rec_Cell_List_cell_xy_N[i][j];
			nrec += n_rec_in_cell;
		}
	}


	if (nrec != total_N_Rec)
	{
		log_out << "Error with rec cell list, Incorrect number  " << nrec << " "  << total_N_Rec << "\n";
	}

	// cleck receptor HS overlap
	for (int rec_i = 0; rec_i < total_N_Rec; rec_i++)
	{
		bool overlap = false;
		double overlap_by = 0;
		for (int i = 0; i < rec_i; i++)
		{
			double dist_2 = dist_2_min_img_xy_rec_rec(Receptors[i], Receptors[rec_i]);

			if (dist_2 < 4 * Rec_HS_rad * Rec_HS_rad) // Hard spheres
			{
				overlap_by = 2 * Rec_HS_rad - sqrt(dist_2);
				overlap = true;
				break;
			}
		}
		if (overlap)
		{
			log_out << "Receptor overlap!! Overlaps by " << overlap_by << ".\nIf reading from system state from file small ovelap may be caused by rouding when writing coordinates to file.\n";
		}
	}



	log_out.close();
}
















