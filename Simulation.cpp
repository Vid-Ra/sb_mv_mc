#include "Simulation.h"

std::array<std::array<std::array<std::array<int, max_blobs_in_cell>, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_ids;
std::array< std::array<int, 3>, max_total_blobs> cell_list_id_cell_xyz;
std::array<std::array<std::array<int, max_cells>, max_cells>, max_cells> cell_list_cell_xyz_N_cell;

std::array<POLYMER, max_poly_in_sim> Polymers{};



extern std::mt19937 generator;




void Simulation::Init_consts()
{
	box_x_2 = box_x / 2.;
	box_y_2 = box_y / 2.;
	box_z_2 = box_z / 2.;
	exp_mu = exp(mu);

	for (int i = 0; i < nr_Lig_types + 1; i++)
	{
		for (int j = 0; j < nr_Rec_types + 1; j++)
		{
			exp_m_Lig_Rec_inter_matrix[i][j] = exp(-Lig_Rec_inter_matrix[i][j]);
		}
	}

	rcut_Lig_Rec_2 = rcut_Lig_Rec * rcut_Lig_Rec;


}


//double dist_ls_calc_angle[3]{};
std::array<double, 3>	 dist_ls_calc_angle{};  // 0 - dist_01,1 - dist_12, 2 - cos_theta_012 


BLOB Simulation::Get_bond_vector(BLOB blob_0, BLOB blob_1)
{
	BLOB vec_01 = { blob_1[0] - blob_0[0],blob_1[1] - blob_0[1],blob_1[2] - blob_0[2] };

	if (vec_01[0] < -box_x_2)
	{
		vec_01[0] += box_x;
	}
	else if (vec_01[0] > box_x_2)
	{
		vec_01[0] -= box_x;
	}

	if (vec_01[1] < -box_y_2)
	{
		vec_01[1] += box_y;
	}
	else if (vec_01[1] > box_y_2)
	{
		vec_01[1] -= box_y;
	}
	return vec_01;
}


double Simulation::calc_cos_bond_angle(BLOB blob_0, BLOB blob_1, BLOB blob_2) 
{
	
	BLOB vec_01 = Get_bond_vector(blob_0, blob_1);

	BLOB vec_12 = Get_bond_vector(blob_1, blob_2);

	double dot_01_12 = dot_prod(vec_01, vec_12);
	double l01 = lenght(vec_01);
	double l12 = lenght(vec_12);
	dist_ls_calc_angle[0] = l01;
	dist_ls_calc_angle[1] = l12;
	double cos_theta = dot_01_12 / (l01 * l12);
	dist_ls_calc_angle[2] = cos_theta;
	return cos_theta;
}

double Simulation::Calculate_bond_bending_interactions_poly(POLYMER poly)
{
	double U_bending = 0;
	for (int bolb_i = 1; bolb_i < Blobs_in_poly - 1; bolb_i++)
	{

		double cos_theta = calc_cos_bond_angle(poly[bolb_i - 1], poly[bolb_i], poly[bolb_i + 1]);
		U_bending += interaction_cos_theta_bending(eps_b, cos_theta);

	}


	return U_bending;
}


double Simulation::Calculate_harmonic_bond_interactions_poly(POLYMER poly)
{
	double U_harmonic = 0;
	for (int blob_i = 1; blob_i < Blobs_in_poly; blob_i++)
	{
		U_harmonic += interaction_Blob_Blob_bond(dist_Blob_min_img_x_y(poly[blob_i], poly[blob_i - 1], box_x, box_x_2, box_y, box_y_2));
	}

	return U_harmonic;
}
double Simulation::Calculate_wall_interactions_blob(BLOB blob)
{
	double U_wall = 0;
	if (blob[2] < rcut_Blob_Wall)
	{
		U_wall +=  interaction_Blob_Wall(blob[2]) - pot_shift_Blob_Wall;
	}
	else if (-blob[2] + box_z < rcut_Blob_Wall)
	{
		U_wall += interaction_Blob_Wall(-blob[2] + box_z) - pot_shift_Blob_Wall;
	}
	return U_wall;
}




double Simulation::Calculate_Interactions_Blob(int poly_id, int blob_id,BLOB curr_trial_blob)
{
	/*
	* Calculates interactions between a blob and other blobs (using cell list), 
	* Used in MC_move_Blob, 
	* does not count blob with poly_id, blob_id
	*
	* 
	*/
	double U_blob_blob = 0;
	double U_harmonic = 0;
	double U_bending = 0;
	double U_wall = 0;


	int cell_x = (int)floor((curr_trial_blob[0] ) / cell_side[0]);
	int cell_y = (int)floor((curr_trial_blob[1] ) / cell_side[1]);
	int cell_z = (int)floor((curr_trial_blob[2] ) / cell_side[2]);
	if (cell_z < 0)
	{
		cell_z = 0;
	}
	else if (cell_z > N_cells[2] - 1)
	{
		cell_z = N_cells[2] - 1;
	}

	// Blob-Blob -Gaussian interaction
	for (int k_cell = cell_z- cell_list_around; k_cell < cell_z + cell_list_around+1; k_cell++)
	{
		if (k_cell < 0 || k_cell > N_cells[2]-1)
		{
			continue;
		}
		for (int j_cell_tmp = cell_y - cell_list_around; j_cell_tmp < cell_y + cell_list_around+1; j_cell_tmp++)
		{
			int j_cell = j_cell_tmp;
			if (j_cell < 0)
			{
				j_cell += N_cells[1];
			}
			else if (j_cell > N_cells[1] - 1)
			{
				j_cell -= N_cells[1];
			}
			for (int i_cell_tmp = cell_x- cell_list_around; i_cell_tmp < cell_x+ cell_list_around+1; i_cell_tmp++)
			{
				
				//int i_cell = i_cell_tmp - N_cells[0] * (int)floor((double)(i_cell_tmp) / N_cells[0] + 1.0e-8); // PBC in x,y
				//int j_cell = j_cell_tmp - N_cells[1] * (int)floor((double)(j_cell_tmp) / N_cells[1] + 1.0e-8);

				
				int i_cell = i_cell_tmp;

				if (i_cell < 0)
				{
					i_cell += N_cells[0];
				}
				else if (i_cell > N_cells[0] - 1)
				{
					i_cell -= N_cells[0];
				}


				for (int cell_blob_i = 0; cell_blob_i < cell_list_cell_xyz_N_cell[i_cell][j_cell][k_cell]; cell_blob_i++)
				{
					int blob_uid1 = cell_list_cell_xyz_ids[i_cell][j_cell][k_cell][cell_blob_i];
					int blob_i = blob_uid1 % Blobs_in_poly;
					int poly_i = (int)floor((double)blob_uid1 / (double)Blobs_in_poly);
					if (!(blob_i == blob_id && poly_i == poly_id))
					{

						double dist_2 = dist_2_Blob_min_img_x_y(curr_trial_blob,Polymers[poly_i][blob_i], box_x, box_x_2, box_y, box_y_2);

						if (dist_2 < rcut_Blob_Blob_2)
						{
							/*
							if (temp_bool)
							{
								std::cout << "cell_list:  " << i_cell << " " << j_cell << " " << k_cell << " " << dist << " " << poly_i << " " << blob_i << "\n";
							}*/
							U_blob_blob += interaction_r2_Blob_Blob(dist_2) - pot_shift_Blob_Blob;
						}
					}
				}
			}
		}
	}


	POLYMER poly = Polymers[poly_id];
	
	// BLOB-BLOB Harmonic interaction
	if (Blobs_in_poly > 1)
	{
		if (blob_id == 0) // "first" blob
		{
			BLOB blob_p1 = poly[blob_id + 1];

			BLOB blob_p2 = poly[blob_id + 2];
			double cos_theta_p012 = calc_cos_bond_angle(curr_trial_blob, blob_p1, blob_p2);
			U_bending += interaction_cos_theta_bending(eps_b, cos_theta_p012);

			//double dist_p11 = dist_Blob_min_img_x_y(curr_trial_blob, blob_p1, box_x, box_x_2, box_y, box_y_2);
			double dist_p1 = dist_ls_calc_angle[0];
			//dist_ls_calc_angle[0] = dist_p1;
			U_harmonic += interaction_Blob_Blob_bond(dist_p1);
		}
		else if (blob_id == Blobs_in_poly - 1) // "last" blob
		{
			BLOB blob_m1 = poly[blob_id - 1];

			BLOB blob_m2 = poly[blob_id - 2];
			double cos_theta_m210 = calc_cos_bond_angle(blob_m2, blob_m1, curr_trial_blob);
			U_bending += interaction_cos_theta_bending(eps_b, cos_theta_m210);

			//double dist_m11 = dist_Blob_min_img_x_y(curr_trial_blob, blob_m1, box_x, box_x_2, box_y, box_y_2);
			double dist_m1 = dist_ls_calc_angle[1];

			dist_ls_calc_angle[0] = dist_m1; // dist_ls_calc_angle[0] is used in average bond dist counting
			U_harmonic += interaction_Blob_Blob_bond(dist_m1);
		}
		else
		{
			BLOB blob_p1 = poly[blob_id + 1];
			//double dist_p11 = dist_Blob_min_img_x_y(curr_trial_blob, blob_p1, box_x, box_x_2, box_y, box_y_2);
			
			BLOB blob_m1 = poly[blob_id - 1];
			//double dist_m11 = dist_Blob_min_img_x_y(curr_trial_blob, blob_m1, box_x, box_x_2, box_y, box_y_2);

			if (blob_id != Blobs_in_poly - 2) // not "second" blob
			{
				BLOB blob_p2 = poly[blob_id + 2];
				double cos_theta_p012 = calc_cos_bond_angle(curr_trial_blob, blob_p1, blob_p2);
				U_bending += interaction_cos_theta_bending(eps_b, cos_theta_p012);

			}
			if (blob_id != 1) // not "second to last" blob
			{
				BLOB blob_m2 = poly[blob_id - 2];
				double cos_theta_m210 = calc_cos_bond_angle(blob_m2, blob_m1,curr_trial_blob );
				U_bending += interaction_cos_theta_bending(eps_b, cos_theta_m210);


			}

			double cos_theta = calc_cos_bond_angle(blob_m1, curr_trial_blob, blob_p1);

			double dist_m1 = dist_ls_calc_angle[0];
			double dist_p1 = dist_ls_calc_angle[1];
			U_harmonic += interaction_Blob_Blob_bond(dist_p1);

			U_harmonic += interaction_Blob_Blob_bond(dist_m1);
			U_bending += interaction_cos_theta_bending(eps_b, cos_theta);
		}
	}
	

	// BLOB-Wall interaction
	U_wall = Calculate_wall_interactions_blob(curr_trial_blob);
	
	//std::cout << "U_bend     " << U_bending  <<" " << U_blob_blob + U_harmonic + U_wall + U_bending << "\n";
	//std::cout<< "int " << U_blob_blob << " " << U_harmonic << " " << U_wall << " " << U_bending << "\n";

	return U_blob_blob + U_harmonic+ U_wall + U_bending;
}

double Simulation::Total_Energy()
{

	double U_blob_blob = 0;
	double U_harmonic = 0;
	double U_bending = 0;
	double U_wall = 0;

	for (int poly_i = 0; poly_i < nr_Poly; poly_i++)
	{

		for (int blob_i = 0; blob_i < Blobs_in_poly; blob_i++)
		{

			// gaussian interactions
			// not using cell lists
			for (int poly_j = 0; poly_j < nr_Poly; poly_j++)
			{
				for (int blob_j = 0; blob_j < Blobs_in_poly; blob_j++)
				{
					if (!(poly_i == poly_j && blob_i == blob_j))
					{
						double dist_2 = dist_2_Blob_min_img_x_y(Polymers[poly_i][blob_i], Polymers[poly_j][blob_j], box_x, box_x_2, box_y, box_y_2);
						if (dist_2 < rcut_Blob_Blob_2)
						{
							U_blob_blob += (interaction_r2_Blob_Blob(dist_2) - pot_shift_Blob_Blob)/2.; // halfed because dobule counting
						}
					}
				}
			}


			U_wall += Calculate_wall_interactions_blob(Polymers[poly_i][blob_i]);

		}

		U_harmonic += Calculate_harmonic_bond_interactions_poly(Polymers[poly_i]);
		U_bending += Calculate_bond_bending_interactions_poly(Polymers[poly_i]);

	}
	double U = U_blob_blob + U_harmonic + U_wall + U_bending;
	//std::cout << "tot " << U_blob_blob << " " << U_harmonic << " " << U_wall << " " << U_bending << "\n";
	return U;
}








void Simulation::Sample_perst_len(int poly_id)
{
	double sum_l_dot_l = 0;
	POLYMER poly = Polymers[poly_id];

	BLOB bond_1 = Get_bond_vector(poly[0],poly[1]);

	for (int i = 0; i < Blobs_in_poly-1; i++)
	{
		BLOB bond_i = Get_bond_vector(poly[i], poly[i+1]);
		double bond_1_dot_bond_i = dot_prod(bond_1, bond_i);
		sum_l_dot_l += bond_1_dot_bond_i;
	}

	sum_sum_l_dot_l += sum_l_dot_l;
	count_sum_l_dot_l++;


}




void Simulation::Sample_end_to_end_dist(int poly_id)
{
	POLYMER poly = Polymers[poly_id];

	double end_end_dist_2 = dist_2_Blob_min_img_x_y(poly[0], poly[Blobs_in_poly - 1], box_x, box_x_2, box_y, box_y_2);


	end_to_end_dist_sum += sqrt(end_end_dist_2);
	end_to_end_dist_2_sum += end_end_dist_2;
	count_end_to_end_dist++;

}

double Simulation::Calc_Rg(int poly_id)
{
	// Rg = 1/2N^2 \sum_i \sum_j (r_i-r_j)^2
	POLYMER poly = Polymers[poly_id];

	double sum = 0;
	for (int i = 0; i < Blobs_in_poly; i++)
	{
		for (int j = 0; j < Blobs_in_poly; j++)
		{
			BLOB ri_m_rj = Get_bond_vector(poly[i], poly[j]);
			double ri_m_rj_2 = dot_prod(ri_m_rj, ri_m_rj);

			sum += ri_m_rj_2;
		}

	}

	return sqrt(sum / 2.) / Blobs_in_poly;
}

void Simulation::Sample_Nr_Lig(int poly_id)
{
	std::array<int ,max_Rec_types + 1> N_lig_type_in_poly_count{};
	for (int ligand_id = 0; ligand_id < Ligands_N_in_Poly[poly_id]; ligand_id++)
	{
		// Ligands_Types_Bonds[poly_id][ligand_id] = {ligand_type,b_receptor_id}
		//  Ligands_in_Poly[poly_id][ligand_id] -> {blob_id,ligand_in_blob_id}
		// Ligands_in_Blob[poly_id][blob_id][0] =  - Nr. ligands in blob_id, Ligands_in_Blob[poly_id][blob_id][ligand_in_blob_id] -> ligand_id
		//Ligands_N_in_Poly

		int lig_type = Ligands_Types_Bonds[poly_id][ligand_id][0];
		N_lig_type_in_poly_count[lig_type]++;

	}

	for (int type = 1; type < nr_Lig_types + 1; type++)
	{

		lig_in_poly_counts_in_sim[type][N_lig_type_in_poly_count[type]]++;
	}

	//

	count_lig_in_poly_sum++;
}


void Simulation::Sample_Rg(int poly_id)
{
	double Rg = Calc_Rg(poly_id);
	Rg_sum += Rg;
	count_Rg_sum++;
}


int close_rec_ids_old[max_rec_in_cell * 9];
double out_rec_dists2_old[max_rec_in_cell * 9];
int close_rec_ids_new[max_rec_in_cell * 9];
double out_rec_dists2_new[max_rec_in_cell * 9];

double Q_o_save[max_rec_in_cell * 9];
double Q_n_save[max_rec_in_cell * 9];

void Simulation::MC_move_Blob()
{
	/* 
	* Standard Metropolis MC move of a blob
	* Integrated with ligand receptor bonding
	*/
	int ran_poly = RandomInt(nr_Poly);
	int ran_blob = RandomInt(Blobs_in_poly); // 2 ran nr., not ideal?

	BLOB current_blob = Polymers[ran_poly][ran_blob]; // chose random blob

	BLOB trial_blob = Random_move(current_blob,max_move_blob, box_x,  box_y); // move the blob to make new trial blob

	double U_old = Calculate_Interactions_Blob(ran_poly, ran_blob, current_blob);
	double old_dist_ls_calc_angle[3];
	for (int i = 0; i < 3; i++)
	{
		old_dist_ls_calc_angle[i] = dist_ls_calc_angle[i];
	}
	double U_new = Calculate_Interactions_Blob(ran_poly, ran_blob, trial_blob); 


	// bonding

	
	int N_ligand_blob = Ligands_in_Blob[ran_poly][ran_blob][0];
	int N_close_rec_old = 0;
	int N_close_rec_new = 0;
	double Q_o = 1;
	double Q_n = 1;

	/*
	if (cyc == 272 && stp == 4)
	{
		int a = 1;
	}
	*/


	if (N_ligand_blob > 0)
	{
		//unbind the blob
		for (int lig_ib_id = 1; lig_ib_id < N_ligand_blob+1; lig_ib_id++)
		{
			int lig_id = Ligands_in_Blob[ran_poly][ran_blob][lig_ib_id];

			int rec_id = Ligands_Types_Bonds[ran_poly][lig_id][1];

			if (rec_id != -1) // ligand is bound, unbind it
			{




				Receptor_Types_Bonds[rec_id][1] = -1;
				Receptor_Types_Bonds[rec_id][2] = -1;
				Ligands_Types_Bonds[ran_poly][lig_id][1] = -1;
				nr_Bonds--;
				int lig_type = Ligands_Types_Bonds[ran_poly][lig_id][0];
				int rec_type = Receptor_Types_Bonds[rec_id][0];
				nr_Bonds_Lig[lig_type]--;
				nr_Bonds_Rec[rec_type]--;
				

				N_bonds_in_chain[ran_poly]--;
				if (N_bonds_in_chain[ran_poly] == 0)
				{
					nr_Bound_Poly--;
				}

				REC bound_rec = Receptors[rec_id];

				double dist2 = dist_2_min_img_xy_blob_rec(current_blob, bound_rec);

				double U_tet = interaction_r2_Bond_Tether(dist2);

				double bond_E = Lig_Rec_inter_matrix[lig_type][rec_type] + U_tet;

				total_bond_U -= bond_E;



			}
		}


		// biding partition f. of old blob
		if (current_blob[2] < rcut_Lig_Rec) // close enough to bond
		{
			N_close_rec_old = Find_Rec_near_Blob(close_rec_ids_old, out_rec_dists2_old, current_blob);
			if (N_ligand_blob == 1) // only case currently
			{
				for (int i = 0; i < N_close_rec_old; i++)
				{
				
						int rec_id = close_rec_ids_old[i];
						int rec_type = Receptor_Types_Bonds[rec_id][0];
					
						int lig_id = Ligands_in_Blob[ran_poly][ran_blob][1];
						int lig_type = Ligands_Types_Bonds[ran_poly][lig_id][0];
						double dist2 = out_rec_dists2_old[i];
						double U_tet = interaction_r2_Bond_Tether(dist2);

						double Q_i = exp_m_Lig_Rec_inter_matrix[lig_type][rec_type] * exp(-U_tet);

						Q_o += Q_i;
						Q_o_save[i] = Q_i;

				}
				for (int i = 0; i < N_close_rec_old; i++)// normalise partition funciton
				{
					Q_o_save[i] /= Q_o;
				}


			}
			else
			{
				std::cout << "MC_move_Blob, N_ligand_blob > 1!!!" << "\n";
			}

		}



		// biding partition f. of new blob

		if (trial_blob[2] < rcut_Lig_Rec) // close enough to bond
		{
			N_close_rec_new = Find_Rec_near_Blob(close_rec_ids_new, out_rec_dists2_new, trial_blob);
			if (N_ligand_blob == 1) // only case currently
			{
				for (int i = 0; i < N_close_rec_new; i++)
				{

					int rec_id = close_rec_ids_new[i];
					int rec_type = Receptor_Types_Bonds[rec_id][0];

					int lig_id = Ligands_in_Blob[ran_poly][ran_blob][1];
					int lig_type = Ligands_Types_Bonds[ran_poly][lig_id][0];
					double dist2 = out_rec_dists2_new[i];
					double U_tet = interaction_r2_Bond_Tether(dist2);

					double Q_i = exp_m_Lig_Rec_inter_matrix[lig_type][rec_type] * exp(-U_tet);


					Q_n += Q_i;
					Q_n_save[i] = Q_i;

				}
				for (int i = 0; i < N_close_rec_new; i++)// normalise partition funciton
				{
					Q_n_save[i] /= Q_n;
				}


			}
			else
			{
				std::cout << "MC_move_Blob, N_ligand_blob > 1!!!" << "\n";
			}

		}
	}












	double dU = U_new - U_old; // energy difference


	double acc = Q_n/Q_o*exp(-dU);
	double ran = RandomDouble01();
	bool accept_bool = ran < acc;
	
	nr_tot_MC_move_Blob++;
	nr_tot_MC_move_Blob_cycle++;
	/*
	if (temp_bool)
	{
		acc = 1;
	}*/
	// metropolis criterion
	if (accept_bool)	//accept

	{
		//double U_old_t = Total_Energy();


		Polymers[ran_poly][ran_blob] = trial_blob;
		total_U += dU;
		nr_acc_MC_move_Blob++;
		nr_acc_MC_move_Blob_cycle++;

		
		int cell_uid = ran_poly * Blobs_in_poly + ran_blob;
		// update cell list
		int x_cell_n = (int)floor((trial_blob[0] ) / cell_side[0]);
		int y_cell_n = (int)floor((trial_blob[1] ) / cell_side[1]);
		int z_cell_n = (int)floor((trial_blob[2] ) / cell_side[2]);
		if (z_cell_n < 0)
		{
			z_cell_n = 0;
		}
		else if (z_cell_n > N_cells[2] - 1)
		{
			z_cell_n = N_cells[2] - 1;
		}
		
		std::array<int, 3> xyz_cell_n = { x_cell_n ,y_cell_n ,z_cell_n };


		std::array<int, 3> xyz_cell_o = cell_list_id_cell_xyz[cell_uid];
		if (xyz_cell_n != xyz_cell_o)
		{

			cell_list_id_cell_xyz[cell_uid] = xyz_cell_n;


			int N_new_cell = cell_list_cell_xyz_N_cell[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]];

			cell_list_cell_xyz_ids[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]][N_new_cell] = cell_uid;

			int N_old_cell = cell_list_cell_xyz_N_cell[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]];

			for (int c_uidi = 0; c_uidi < N_old_cell; c_uidi++)
			{
				if (cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][c_uidi] == cell_uid)
				{
					cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][c_uidi] = cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][N_old_cell - 1];
					cell_list_cell_xyz_ids[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]][N_old_cell - 1] = -1;
					break;
				}
			}


			cell_list_cell_xyz_N_cell[xyz_cell_n[0]][xyz_cell_n[1]][xyz_cell_n[2]]++;
			cell_list_cell_xyz_N_cell[xyz_cell_o[0]][xyz_cell_o[1]][xyz_cell_o[2]]--;
		}
		
		//double U_new_t = Total_Energy();
		//int tm = 0;

		
	}


	// Rebond
	if (N_ligand_blob > 0)
	{
		if (accept_bool) // bond from new
		{
			if (trial_blob[2] < rcut_Lig_Rec) // close enough to bond
			{
				double arg_bond = 1 / Q_n;
				double rn = RandomDouble01();
				if (rn > arg_bond) //make a bond
				{
					for (int i = 0; i < N_close_rec_new; i++)
					{
						arg_bond += Q_n_save[i];

						if (rn < arg_bond) // bond this one
						{
							int rec_id = close_rec_ids_new[i];

							int lig_id = Ligands_in_Blob[ran_poly][ran_blob][1]; // only 1 ligand in blob!!!
							Ligands_Types_Bonds[ran_poly][lig_id][1] = rec_id;
							Receptor_Types_Bonds[rec_id][1] = ran_poly;
							Receptor_Types_Bonds[rec_id][2] = lig_id;
							nr_Bonds++;
							int lig_type = Ligands_Types_Bonds[ran_poly][lig_id][0];
							int rec_type = Receptor_Types_Bonds[rec_id][0];
							nr_Bonds_Lig[lig_type]++;
							nr_Bonds_Rec[rec_type]++;

							if (N_bonds_in_chain[ran_poly] == 0)
							{
								nr_Bound_Poly++;
							}
							N_bonds_in_chain[ran_poly]++;

							double bond_U = -log(Q_n_save[i] * Q_n);

							total_bond_U += bond_U;
							/*
							BLOB bound_blob = Polymers[ran_poly][ran_blob];
							REC bound_rec = Receptors[rec_id];

							double dist2 = dist_2_min_img_xy_blob_rec(bound_blob, bound_rec);

							double U_tet = interaction_r2_Bond_Tether(dist2);

							double bond_E = Lig_Rec_inter_matrix[lig_type][rec_type] + U_tet;*/
							break;
						}
					}
				}

			}
		}
		else // bond from old
		{
			if (current_blob[2] < rcut_Lig_Rec) // close enough to bond
			{
				double arg_bond = 1 / Q_o;
				double rn = RandomDouble01();
				if (rn > arg_bond) //make a bond
				{
					for (int i = 0; i < N_close_rec_old; i++)
					{
						arg_bond += Q_o_save[i];

						if (rn < arg_bond) // bond this one
						{
							int rec_id = close_rec_ids_old[i];

							int lig_id = Ligands_in_Blob[ran_poly][ran_blob][1]; // only 1 ligand in blob!!!
							Ligands_Types_Bonds[ran_poly][lig_id][1] = rec_id;
							Receptor_Types_Bonds[rec_id][1] = ran_poly;
							Receptor_Types_Bonds[rec_id][2] = lig_id;
							nr_Bonds++;
							int lig_type = Ligands_Types_Bonds[ran_poly][lig_id][0];
							int rec_type = Receptor_Types_Bonds[rec_id][0];
							nr_Bonds_Lig[lig_type]++;
							nr_Bonds_Rec[rec_type]++;

							if (N_bonds_in_chain[ran_poly] == 0)
							{
								nr_Bound_Poly++;
							}
							N_bonds_in_chain[ran_poly]++;
							double bond_U = -log(Q_o_save[i] * Q_o);
							total_bond_U += bond_U;

							/*
							BLOB bound_blob = Polymers[ran_poly][ran_blob];
							REC bound_rec = Receptors[rec_id];

							double dist2 = dist_2_min_img_xy_blob_rec(bound_blob, bound_rec);

							double U_tet = interaction_r2_Bond_Tether(dist2);

							double bond_E = Lig_Rec_inter_matrix[lig_type][rec_type] + U_tet;*/
							break;
						}
					}
				}

			}
		}
	}

	



	if (cyc >= ekv_cycles) // log_outing cos(theta), blob-blob bond distance
	{
		dist_sum_nr++;
		if (ran_blob == 0 || ran_blob == Blobs_in_poly - 1) // did not calculate angle, only one distance
		{


			if (accept_bool)
			{
				dist_blob_blob_bond_sum += dist_ls_calc_angle[0];
				int nr_dist_bin = (int)floor(dist_ls_calc_angle[0] / dist_bin_width);
				if (nr_dist_bin < total_nr_dist_bins)
				{
					dist_blob_blob_bond_bins[nr_dist_bin] += 1;

				}
			}
			else
			{
				dist_blob_blob_bond_sum += old_dist_ls_calc_angle[0];
				int nr_dist_bin = (int)floor(old_dist_ls_calc_angle[0] / dist_bin_width);
				if (nr_dist_bin < total_nr_dist_bins)
				{
					dist_blob_blob_bond_bins[nr_dist_bin] += 1;

				}
			}
		}
		else
		{
			cos_theta_sum_nr++;
			if (accept_bool)
			{
				double cos_theta = dist_ls_calc_angle[2];

				double avg_dist = (dist_ls_calc_angle[0] + dist_ls_calc_angle[1] ) / 2;
				dist_blob_blob_bond_sum += avg_dist;
				cos_theta_sum += cos_theta;

				int nr_dist_bin = (int)floor(avg_dist / dist_bin_width);
				if (nr_dist_bin < total_nr_dist_bins)
				{
					dist_blob_blob_bond_bins[nr_dist_bin] += 1;

				}
				int nr_cos_theta_bin = (int)floor((cos_theta - start_cos_theta_bin) / cos_theta_bin_width);
				if (nr_cos_theta_bin < total_nr_cos_theta_bins)
				{
					cos_theta_bins[nr_cos_theta_bin] += 1;

				}
				double theta = acos(cos_theta);
				theta_sum += theta;
				int nr_theta_bin = (int)floor(theta / theta_bin_width);
				if (nr_theta_bin < total_nr_theta_bins)
				{
					theta_bins[nr_theta_bin] += 1;

				}


			}
			else
			{
				double cos_theta = old_dist_ls_calc_angle[2];
				double avg_dist = (old_dist_ls_calc_angle[0] + old_dist_ls_calc_angle[1] ) / 2;
				dist_blob_blob_bond_sum += avg_dist;
				cos_theta_sum += cos_theta;
				int nr_dist_bin = (int)floor(avg_dist / dist_bin_width);
				if (nr_dist_bin < total_nr_dist_bins)
				{
					dist_blob_blob_bond_bins[nr_dist_bin] += 1;

				}
				int nr_cos_theta_bin = (int)floor((cos_theta - start_cos_theta_bin) / cos_theta_bin_width);
				if (nr_cos_theta_bin < total_nr_cos_theta_bins)
				{
					cos_theta_bins[nr_cos_theta_bin] += 1;

				}
				double theta = acos(cos_theta);
				theta_sum += theta;
				int nr_theta_bin = (int)floor(theta / theta_bin_width);
				if (nr_theta_bin < total_nr_theta_bins)
				{
					theta_bins[nr_theta_bin] += 1;
				}



			}
		}
	}

	
	//Check_Bonds();
	//int N_ligands = 0;

}




void Simulation::Run_Sim()
{

	total_U = Total_Energy();
	total_bond_U = Calc_bond_energy();
	total_rec_U = Total_Rec_Energy();

	uint64_t out_cycles = (uint64_t)(total_nr_cycles * out_frac_cycles+0.5);

	Start_Output();
	double nr_Poly_cycle_sum = 0;
	double total_U_cycle_sum = 0;

	double nr_Poly_out_cycles_sum = 0;
	double total_U_out_cycles_sum = 0;

	double nr_ekv_cycles = 0;


	double nr_Bonds_out_cycles_sum = 0;
	double nr_Bonds_cycle_sum = 0;

	double nr_Bound_Poly_out_cycles_sum = 0;
	double nr_Bound_Poly_cycle_sum = 0;

	using std::chrono::high_resolution_clock;
	using std::chrono::duration_cast;
	using std::chrono::duration;
	using std::chrono::milliseconds;
	auto t1_total = high_resolution_clock::now();

	auto t1 = high_resolution_clock::now();
	auto t2 = high_resolution_clock::now();

	std::array<double, max_Rec_types + 1> nr_Bonds_Rec_cycle_sum = {};
	std::array<double, max_Rec_types + 1> nr_Bonds_Lig_cycle_sum = {};


	double frac_ins_del_rec_moves = frac_ins_del_moves + frac_rec_moves;
	

	for (uint64_t cycle = 1; cycle < total_nr_cycles+1; cycle++) // main cycle
	{
		
		if (exit_signal)
			break;

		for (int step = 0; step < Blobs_in_poly; step++)
		{
			cyc = cycle;
			stp = step;
			double ran = RandomDouble01();

			if (ran < frac_ins_del_moves)
			{
				//std::cout << "RB_Ins_Del_MC_move " << cycle << " " << step << "\n";

				RB_Ins_Del_MC_move(); 
				//Check_Cell_List();
				//std::cout << "RB_Ins_Del_MC_move " << cycle << " " << step << "\n";

			}
			else if (ran < frac_ins_del_rec_moves)
			{
				if (do_Rec_LJ_inter)
				{
					MC_move_Rec_LJ();
				}
				else
				{
					MC_move_Rec_HS();
				}
			}
			else
			{
				if (nr_Poly > 0)
				{
					MC_move_Blob();
					//std::cout << "MC_move_Blob " << cycle << " " << step << "\n";
					//Check_Cell_List();

				}
			}




		}

		
		


		if (cycle >= ekv_cycles)
		{
			nr_ekv_cycles++;
			nr_Poly_cycle_sum += nr_Poly;
			nr_Bound_Poly_cycle_sum += nr_Bound_Poly;
			if (nr_Poly > 0)
			{
				total_U_cycle_sum += total_U / nr_Poly;
				nr_Bonds_cycle_sum += nr_Bonds / nr_Poly;

				for (int i = 1; i < nr_Lig_types+1; i++)
				{
					nr_Bonds_Lig_cycle_sum[i] += nr_Bonds_Lig[i] / nr_Poly;
				}
				for (int i = 1; i < nr_Rec_types + 1; i++)
				{
					nr_Bonds_Rec_cycle_sum[i] += nr_Bonds_Rec[i] / nr_Poly;
				}
				int ran_poly = RandomInt(nr_Poly);
				Sample_perst_len(ran_poly);
				Sample_end_to_end_dist(ran_poly);
				Sample_Rg(ran_poly);
				Sample_Nr_Lig(ran_poly);
			}




		}
		nr_Poly_out_cycles_sum += nr_Poly;
		nr_Bound_Poly_out_cycles_sum += nr_Bound_Poly;

		if (nr_Poly>0)
		{
			total_U_out_cycles_sum += total_U / nr_Poly;
			nr_Bonds_out_cycles_sum += nr_Bonds / nr_Poly;

		}



		if (cycle % out_cycles == 0) // output
		{

			/*
			double te1 = Total_Energy();
			double te2 = Total_Energy_no_cell_list();
			Check_Cell_List();
			double ter = Total_Rec_Energy();
			*/

			t2 = high_resolution_clock::now();
			
			Check_Ligand_lists();
			Check_Cell_List();
			Check_Bonds(); 
			Check_Rec_Cell_List();

			duration<double, std::milli> ms_double = t2 - t1;
			double exe_time = ms_double.count()/1000.0;
			double avg_nr_poly_out_cycle = nr_Poly_out_cycles_sum / out_cycles;
			double avg_U_total_per_poly_out_cycle = total_U_out_cycles_sum / out_cycles;
			double avg_nr_Bonds_per_poly_per_out_cycle = nr_Bonds_out_cycles_sum / out_cycles;
			double avg_nr_Bound_Poly_out_cycle = nr_Bound_Poly_out_cycles_sum / out_cycles;
			Output(cycle, avg_nr_poly_out_cycle, avg_U_total_per_poly_out_cycle,exe_time, avg_nr_Bonds_per_poly_per_out_cycle, avg_nr_Bound_Poly_out_cycle);
			nr_Poly_out_cycles_sum = 0;
			total_U_out_cycles_sum = 0;
			nr_Bonds_out_cycles_sum = 0;
			nr_Bound_Poly_out_cycles_sum = 0;

			nr_acc_MC_move_Blob_cycle = 0;
			nr_acc_MC_RB_ins_cycle = 0;
			nr_acc_MC_RB_del_cycle = 0;
			nr_acc_MC_move_Rec_cycle = 0;

			nr_tot_MC_move_Blob_cycle = 0;
			nr_tot_MC_RB_ins_cycle = 0;
			nr_tot_MC_RB_del_cycle = 0;
			nr_tot_MC_move_Rec_cycle = 0;
			t1 = high_resolution_clock::now();
		}
	} // end
	t2 = high_resolution_clock::now();
	duration<double, std::milli> ms_double = t2 - t1_total;
	double total_exe_time = ms_double.count() / 1000.0;
	// Normalizations of sums
	double avg_nr_poly = nr_Poly_cycle_sum / nr_ekv_cycles;
	double avg_U_total_per_poly = total_U_cycle_sum / nr_ekv_cycles;
	double avg_nr_Bonds_per_poly = nr_Bonds_cycle_sum / nr_ekv_cycles;
	double avg_nr_Bound_Poly = nr_Bound_Poly_cycle_sum / nr_ekv_cycles;
	for (int i = 1; i < max_Rec_types + 1; i++)
	{
		nr_Bonds_Lig_cycle_sum[i] /= nr_ekv_cycles;
		nr_Bonds_Rec_cycle_sum[i] /= nr_ekv_cycles;
	}


	// dist bins
	dist_blob_blob_bond_sum /= dist_sum_nr;
	for (int i = 0; i < total_nr_dist_bins; i++)
	{
		dist_blob_blob_bond_bins[i] /= dist_sum_nr;
	}
	// cos(theta) bins
	cos_theta_sum /= cos_theta_sum_nr;
	for (int i = 0; i < total_nr_cos_theta_bins; i++)
	{
		cos_theta_bins[i] /=  cos_theta_sum_nr;
	}	
	// (theta) bins
	theta_sum /= cos_theta_sum_nr;
	for (int i = 0; i < total_nr_theta_bins; i++)
	{
		theta_bins[i] /= cos_theta_sum_nr;
	}
	// sum_l_dot_l
	sum_sum_l_dot_l /= count_sum_l_dot_l;
	// end_to_end_dist
	end_to_end_dist_sum /= count_end_to_end_dist;
	end_to_end_dist_2_sum /= count_end_to_end_dist;

	Rg_sum /= count_Rg_sum;

	End_Output(avg_nr_poly, avg_U_total_per_poly,total_exe_time, avg_nr_Bonds_per_poly, nr_Bonds_Lig_cycle_sum, nr_Bonds_Rec_cycle_sum, avg_nr_Bound_Poly);
	Check_Cell_List();


}

