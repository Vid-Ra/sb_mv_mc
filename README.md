# SB_MV_MC

Soft Blob Multivalent Monte Carlo

## Compile:
- g++ -o SB_MV_MC.out -std=gnu++11  -O3 main.cpp Blob.cpp Simulation.cpp random.cpp Sim_Cell_List.cpp Sim_RB_Ins_Del.cpp Sim_Input_Output.cpp Sim_Lig_Rec.cpp

## Input file 
SB_MV_MC.inp:
- ------------Soft Blob Multvalent Monte Carlo Input----------
- -------------Simulation Lenght parameters-------------------
- total_nr_cycles 			        Total number of MC cycles
- out_frac_cycles 			        Fraction of cycles per output
- ekv_frac					        Ekvalibration fraction of total cycles
- output_last_conf                    1 - Output the final configuration of Polymers and ligands as system.pdb
- ------------Simulation parameters--------------------------
- box_x box_y box_z			        Sumulation box X Y Z
- mu							    Chemical potential of ideal chains
- eps_b                             Bending potential strenght      
- k 							        Number of trial moves in RB scheme
- max_move_blob				        Maximum displacement of blob in MC moves
- frac_ins_del_moves frac_rec_moves	Fraction of MC moves that insert/delete polymers  and fraction of receptor moves
- max_move_rec				        Maximum displacement of receptors in MC receptor moves
- random_seed					        Seed for random numbers, if set to -1 use current time
- ----------Polymer, Ligand and Receptor Parameters----------
- nr_Lig_types nr_Rec_types	        Number of Ligand, Receptror types
- Blobs_in_poly				        Number of blobs in Polymer
- N_lig_per_type_per_poly		        Number of ligands per type in polymer, nr_Lig_types values
- block_copoly poiss_ligand_dist      0 - Random copolymer, 1 block copolymer, works for 2 lig types, same number of them on poly, poiss_ligand_dist numbers of ligands are averages, possion - - - - - distrubuted on polymers
- N_rec_per_type				        Number of receptors per type, nr_Rec_types values
- rcut_Lig_Rec                        Ligand Receptor bond cutoff
- Rec_HS_rad                          Receptor hard sphere radius, only between receptors
- ------- Lig-Rec interaction matrix			
- e11 e12						        nr_Lig_types x nr_Rec_types matrix of interactions between types of ligands and receptors
- e21 e22
---------------- Rec-Rec LJ interactions -----------------
- do_Rec_LJ_inter                     if true, adds LJ interactions to  receptors
- nr_Rec_types                        sigma parameter of LJ receptor potential
---------------- Rec-Rec LJ interaction matrix -----------------
- eps_LJ_11 eps_LJ_12                 nr_Rec_types x nr_Rec_types matrix of interactions between types of receptors
- eps_LJ_21 eps_LJ_22
============================================================

